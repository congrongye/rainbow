module RREX(

	input			i_RREX_clk,
	input			i_RREX_enable,
	input			i_RREX_reset,
	
	input			i_RREX_valid,
	input	[31:0]	i_RREX_PC,
	input	[31:0]	i_RREX_Inst,
	input	[4:0]	i_RREX_alcode,
	input	[4:0]	i_RREX_mdcode,
	input	[4:0]	i_RREX_memcode,
	input	[31:0]	i_RREX_operand1,
	input	[31:0]	i_RREX_operand2,
	input	[63:0]	i_RREX_HILOdata,
	input	[4:0]	i_RREX_regdes,
	input	[5:0]	i_RREX_pregdes,
	input	[5:0]	i_RREX_ppregdes,
	input	[5:0]	i_RREX_ReOrderNum,
	input	[5:0]	i_RREX_EXCCODE,
	
	output			o_RREX_valid,
	output	[31:0]	o_RREX_PC,
	output	[31:0]	o_RREX_Inst,
	output	[4:0]	o_RREX_alcode,
	output	[4:0]	o_RREX_mdcode,
	output	[4:0]	o_RREX_memcode,
	output	[31:0]	o_RREX_operand1,
	output	[31:0]	o_RREX_operand2,
	output	[63:0]	o_RREX_HILOdata,
	output	[4:0]	o_RREX_regdes,
	output	[5:0]	o_RREX_pregdes,
	output	[5:0]	o_RREX_ppregdes,
	output	[5:0]	o_RREX_ReOrderNum,
	output	[7:0]	o_RREX_EXCCODE

);

	reg			valid;
	reg	[31:0] 	PC;
	reg	[31:0] 	Inst;
	reg	[4:0] 	alcode;
	reg	[4:0] 	mdcode;
	reg	[4:0] 	memcode;
	reg	[31:0] 	operand1;
	reg	[31:0] 	operand2;
	reg	[63:0] 	HILOdata;
	reg	[4:0] 	regdes;
	reg	[5:0] 	pregdes;
	reg	[5:0] 	ppregdes;
	reg	[5:0] 	ReOrderNum;
	reg	[7:0] 	EXCCODE;

always @(posedge i_RREX_clk)
	begin
		if(i_RREX_reset)
			begin
				valid		<= 6'd0;
				PC			<= 32'd0;
				Inst		<= 32'd0;
				alcode		<= 5'd0;
				mdcode		<= 5'd0;
				memcode		<= 5'd0;
				operand1	<= 32'd0;
				operand2	<= 32'd0;
				HILOdata	<= 63'd0;
				regdes		<= 5'd0;
				pregdes		<= 6'd0;
				ppregdes	<= 6'd0;
				ReOrderNum	<= 6'd0;
				EXCCODE		<= 7'd0;	
			end
		else if(i_RREX_enable)
			begin
				valid		<= i_RREX_valid;
				PC			<= i_RREX_PC;
				Inst		<= i_RREX_Inst;
				alcode		<= i_RREX_alcode;
				mdcode		<= i_RREX_mdcode;
				memcode		<= i_RREX_memcode;
				operand1	<= i_RREX_operand1;
				operand2	<= i_RREX_operand2;
				HILOdata	<= i_RREX_HILOdata;
				regdes		<= i_RREX_regdes;
				pregdes		<= i_RREX_pregdes;
				ppregdes	<= i_RREX_ppregdes;
				ReOrderNum	<= i_RREX_ReOrderNum;
				EXCCODE		<= i_RREX_EXCCODE;
			end
	end

endmodule