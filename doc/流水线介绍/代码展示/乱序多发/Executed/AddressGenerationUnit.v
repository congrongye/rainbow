module AddressGenerationUnit(

	input	[4:0]	i_AGU_memcode,
	input	[31:0]	i_AGU_base,
	input	[15:0]	i_AGU_imm,
	
	output	[31:0]	o_AGU_Vaddr,
	output	[2:0]	o_AGU_Size

);
	
	wire	[31:0]	offset = {16{i_AGU_imm[15]}, i_AGU_imm};
	assign	o_AGU_Vaddr = i_AGU_base + offset;
	
	wire	LW	= (i_AGU_memcode == 5'd1);
	wire	LH	= (i_AGU_memcode == 5'd9);
	wire	LHU	= (i_AGU_memcode == 5'd11);
	wire	LB	= (i_AGU_memcode == 5'd10);
	wire	LBU	= (i_AGU_memcode == 5'd12);
	wire	SW	= (i_AGU_memcode == 5'd5);
	wire	SH	= (i_AGU_memcode == 5'd13);
	wire	SB	= (i_AGU_memcode == 5'd14);
	
	wire	[2:0]	SizeW	= (LW | SW) ? 3'd4 : 3'd0;
	wire	[2:0]	SizeH	= (LH | SH | LHU) ? 3'd2 : 3'd0;
	wire	[2:0]	SizeB	= (LB | SB | LBU) ? 3'd1 : 3'd0;
	

endmodule