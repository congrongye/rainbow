module myCPU_core(

	input			i_CORE_clk,
	input			i_CORE_enable,
	input			i_CORE_reset,
	
	input	[5:0]	i_CORE_interrupt,
	
	output			o_CORE_ren1,
	output			o_CORE_wen1,
	output	[31:0]	o_CORE_addr1,
	output	[2:0]	o_CORE_size1,
	output	[31:0]	o_CORE_wdata1,
	input	[31:0]	i_CORE_rdata1,
	input			i_CORE_dataok1,
	
	output			o_CORE_ren2,
	output			o_CORE_wen2,
	output	[31:0]	o_CORE_addr2,
	output	[2:0]	o_CORE_size2,
	output	[31:0]	o_CORE_wdata2,
	input	[31:0]	i_CORE_rdata2,
	input			i_CORE_dataok2,
	
	output			o_CORE_ren3,
	output			o_CORE_wen3,
	output	[31:0]	o_CORE_addr3,
	output	[2:0]	o_CORE_size3,
	output	[31:0]	o_CORE_wdata3,
	input	[31:0]	i_CORE_rdata3,
	input			i_CORE_dataok3
	
);

	wire	RM_stall;
	wire	IQ_stall;
	wire	LSQ_stall;
	wire	MQ_stall;
	wire	MDU_stall;
	wire	Load_stall;
	wire	SB_stall;
	wire	ROB_stall;
	
	wire	[31:0]	RR_Status;
	wire	[31:0]	RR_EPC;
	wire			RR_interrupt;
	
	/*
		<--------InstFetch-------->
	*/
	
	reg			IF_reset_rec;
	reg	[31:0]	IF_PC;
	
	wire			IF_clk = i_CORE_clk;
	wire			IF_reset;
	wire			IF_enable;
	
	wire	[31:0]	IF_PC_next;
	
always @(IF_clk)
	begin
		if(IF_reset)
			begin
				IF_PC 			<= 32'hbfc0_0000;
				IF_reset_rec 	<= 1'd1;
			end
		else if(IF_enable)
			begin
				IF_PC 			<= IF_PC_next;
				IF_reset_rec 	<= 1'd0;
			end
	end
	
ProgramCounterSelector CORE_PCS(
	
	//PCcurrent 
	.i_PCS_PCcurrent(	IF_PC),
	//COMMIT
	.i_PCS_EPC(			),	
	.i_PCS_EXCCODE(		),	//{[7]valid, [6]isERET, [5]isDelaySlot, [4:0]ExcCode}
	.i_PCS_BRANCH(		),	//{[34]BranchEnable, [33:32]BranchTaken, [31:0]BranchTarget}
	.i_PCS_interrupt(	),
	//PreDecoder
	.i_PCS_PC4(			),
	//BTB
	.i_PCS_BTBhit(		),
	.i_PCS_BTBcounter(	),
	.i_PCS_BTBtarget(	),
	//PCnext
	.o_PCS_PCnext(		IF_PC_next)
	
);

BranchTargetBuffer CORE_BTB(

	.i_BTB_clk(),
	.i_BTB_enable(),
	.i_BTB_reset(),
	//EnQueue
	.i_BTB_PDCvalid(),
	.i_BTB_PDClikely(),
	.i_BTB_PDCPC(),
	.i_BTB_PDCtarget(),
	//Update
	.i_BTB_CMTvalid(),
	.i_BTB_CMTtaken(),
	.i_BTB_CMTPC(),
	.i_BTB_CMTtarget(),
	//LookUp
	.i_BTB_LUPPC(),
	.o_BTB_LUPhit(),
	.o_BTB_LUPcounter(),
	.o_BTB_LUPtarget()
	
);

MemoryManagementUnit CORE_IMMU(

	.i_MMU_Vaddr(),
	.o_MMU_Paddr()

);

	/*
		<--------Pre_InstDecode-------->
	*/
IFPID CORE_IFPID(
	
	.i_IFPID_clk(),
	.i_IFPID_enable(),
	.i_IFPID_reset(),
	
	.i_IFPID_valid(),
	.i_IFPID_PC(),
	.i_IFPID_BranchHit(),
	.i_IFPID_BranchTaken(),
	.i_IFPID_BranchTarget(),
	
	.o_IFPID_valid(),
	.o_IFPID_PC(),
	.o_IFPID_BranchHit(),
	.o_IFPID_BranchTaken(),
	.o_IFPID_BranchTarget()
	
);

PreDecoder CORE_PDC(

	.i_PDC_PC1(),
	.i_PDC_Inst1(),
	.i_PDC_PC2(),
	.i_PDC_Inst2(),
	
	//Inst1
	.o_PDC_BJvalid1(),
	.o_PDC_likely1(),
	.o_PDC_PC1(),
	.o_PDC_target1(),
	.o_PDC_Inst1(),
	//Inst2
	.o_PDC_BJvalid2(),
	.o_PDC_likely2(),
	.o_PDC_PC2(),
	.o_PDC_target2(),
	.o_PDC_Inst2()
	
);
	
	/*
		<--------InstDecode-------->
	*/
	
	wire			ID_clk = i_CORE_clk;
	wire			ID_reset;
	wire			ID_enable;
	//Inst1
	wire			ID_valid1;
	wire	[31:0]	ID_PC1;
	wire	[31:0]	ID_Inst1;
	wire	[4:0]	ID_BranchJump1;
	wire			ID_BranchDelaySlot1;
	wire	[1:0]	ID_BranchTaken1;
	wire	[31:0]	ID_BranchTarget1;
	//Inst2
	wire			ID_valid2;
	wire	[31:0]	ID_PC2;
	wire	[31:0]	ID_Inst2;
	wire	[4:0]	ID_BranchJump2;
	wire			ID_BranchDelaySlot2;
	wire	[1:0]	ID_BranchTaken2;
	wire	[31:0]	ID_BranchTarget2;
	
PIDID CORE_PIDID1(
	
	.i_PIDID_clk(				ID_clk),
	.i_PIDID_enable(			ID_enable),
	.i_PIDID_reset(				ID_reset),
	
	.i_PIDID_valid(				),
	.i_PIDID_PC(				),
	.i_PIDID_Inst(				),
	.i_PIDID_BranchJump(		),
	.i_PIDID_BranchDelaySlot(	),
	.i_PIDID_BranchTaken(		),
	.i_PIDID_BranchTarget(		),
	
	.o_PIDID_valid(				ID_valid1),
	.o_PIDID_PC(				ID_PC1),
	.o_PIDID_Inst(				ID_Inst1),
	.o_PIDID_BranchJump(		ID_BranchJump1),
	.o_PIDID_BranchDelaySlot(	ID_BranchDelaySlot1),
	.o_PIDID_BranchTaken(		ID_BranchTaken1),
	.o_PIDID_BranchTarget(		ID_BranchTarget1)
	
);

PIDID CORE_PIDID2(
	
	.i_PIDID_clk(				ID_clk),
	.i_PIDID_enable(			ID_enable),
	.i_PIDID_reset(				ID_reset),
	
	.i_PIDID_valid(				),
	.i_PIDID_PC(				),
	.i_PIDID_Inst(				),
	.i_PIDID_BranchJump(		),
	.i_PIDID_BranchDelaySlot(	),
	.i_PIDID_BranchTaken(		),
	.i_PIDID_BranchTarget(		),
	
	.o_PIDID_valid(				ID_valid2),
	.o_PIDID_PC(				ID_PC2),
	.o_PIDID_Inst(				ID_Inst2),
	.o_PIDID_BranchJump(		ID_BranchJump2),
	.o_PIDID_BranchDelaySlot(	ID_BranchDelaySlot2),
	.o_PIDID_BranchTaken(		ID_BranchTaken2),
	.o_PIDID_BranchTarget(		ID_BranchTarget2)
	
);

	wire			ID_EXL;
	//Inst1
	wire	[4:0]	ID_alcode1;
	wire	[4:0]	ID_bjcode1;
	wire	[4:0]	ID_iecode1;
	wire	[4:0]	ID_mvcode1;
	wire	[4:0]	ID_mdcode1;
	wire	[4:0]	ID_memcode1;
	wire	[4:0]	ID_EXCCODE1;
	wire	[4:0]	ID_regsrc1;
	wire	[4:0]	ID_regsrc2;
	wire	[4:0]	ID_regdes1;
	//Inst2
	wire	[4:0]	ID_alcode2;
	wire	[4:0]	ID_bjcode2;
	wire	[4:0]	ID_iecode2;
	wire	[4:0]	ID_mvcode2;
	wire	[4:0]	ID_mdcode2;
	wire	[4:0]	ID_memcode2;
	wire	[4:0]	ID_EXCCODE2;
	wire	[4:0]	ID_regsrc3;
	wire	[4:0]	ID_regsrc4;
	wire	[4:0]	ID_regdes2;
	
Decoder CORE_DC1(

	.i_DC_valid(			ID_valid1),
	.i_DC_Inst(				ID_Inst1),
	.i_DC_EXL(				ID_EXL),
	.i_DC_BranchDelaySlot(	ID_BranchDelaySlot1),
	
	.o_DC_alcode(			ID_alcode1),
	.o_DC_bjcode(			ID_bjcode1),
	.o_DC_iecode(			ID_iecode1),
	.o_DC_mvcode(			ID_mvcode1),
	.o_DC_mdcode(			ID_mdcode1),
	.o_DC_memcode(			ID_memcode1),
	.o_DC_EXCCODE(			ID_EXCCODE1),	//{[7]valid, [6]isERET, [5]isDelaySlot, [4:0]ExcCode}
	.o_DC_regsrc1(			ID_regsrc1),
	.o_DC_regsrc2(			ID_regsrc2),
	.o_DC_regdes(			ID_regdes1)
	
);

Decoder CORE_DC2(

	.i_DC_valid(			ID_valid2),
	.i_DC_Inst(				ID_Inst2),
	.i_DC_EXL(				ID_EXL),
	.i_DC_BranchDelaySlot(	ID_BranchDelaySlot2),
	
	.o_DC_alcode(			ID_alcode2),
	.o_DC_bjcode(			ID_bjcode2),
	.o_DC_iecode(			ID_iecode2),
	.o_DC_mvcode(			ID_mvcode2),
	.o_DC_mdcode(			ID_mdcode2),
	.o_DC_memcode(			ID_memcode2),
	.o_DC_EXCCODE(			ID_EXCCODE2),	//{[7]valid, [6]isERET, [5]isDelaySlot, [4:0]ExcCode}
	.o_DC_regsrc1(			ID_regsrc3),
	.o_DC_regsrc2(			ID_regsrc4),
	.o_DC_regdes(			ID_regdes2)
	
);

	/*
		<--------ReName-------->
	*/
	
	wire			RN_clk = i_CORE_clk;
	wire			RN_reset;
	wire			RN_enable;
	//Inst1
	wire			RN_valid1;
	wire	[31:0]	RN_PC1;
	wire	[31:0]	RN_Inst1;
	wire	[4:0]	RN_alcode1;
	wire	[4:0]	RN_bjcode1;
	wire	[4:0]	RN_iecode1;
	wire	[4:0]	RN_mvcode1;
	wire	[4:0]	RN_mdcode1;
	wire	[4:0]	RN_memcode1;
	wire	[4:0]	RN_regsrc1;
	wire	[4:0]	RN_regsrc2;
	wire	[4:0]	RN_regdes1;
	wire	[1:0]	RN_BranchTaken1;
	wire	[31:0]	RN_BranchTarget1;
	wire	[7:0]	RN_EXCCODE1;
	//Inst2
	wire			RN_valid2;
	wire	[31:0]	RN_PC2;
	wire	[31:0]	RN_Inst2;
	wire	[4:0]	RN_alcode2;
	wire	[4:0]	RN_bjcode2;
	wire	[4:0]	RN_iecode2;
	wire	[4:0]	RN_mvcode2;
	wire	[4:0]	RN_mdcode2;
	wire	[4:0]	RN_memcode2;
	wire	[4:0]	RN_regsrc3;
	wire	[4:0]	RN_regsrc4;
	wire	[4:0]	RN_regdes2;
	wire	[1:0]	RN_BranchTaken2;
	wire	[31:0]	RN_BranchTarget2;
	wire	[7:0]	RN_EXCCODE2;
	
IDRN CORE_IDRN1(
	
	.i_IDRN_clk(			RN_clk),
	.i_IDRN_enable(			RN_enable),
	.i_IDRN_reset(			RN_reset),
	
	.i_IDRN_valid(			ID_valid1),
	.i_IDRN_PC(				ID_PC1),
	.i_IDRN_Inst(			ID_Inst1),
	.i_IDRN_alcode(			ID_alcode1),
	.i_IDRN_bjcode(			ID_bjcode1),
	.i_IDRN_iecode(			ID_iecode1),
	.i_IDRN_mvcode(			ID_mvcode1),
	.i_IDRN_mdcode(			ID_mdcode1),
	.i_IDRN_memcode(		ID_memcode1),
	.i_IDRN_regsrc1(		ID_regsrc1),
	.i_IDRN_regsrc2(		ID_regsrc2),
	.i_IDRN_regdes(			ID_regdes1),
	.i_IDRN_BranchTaken(	ID_BranchTaken1),
	.i_IDRN_BranchTarget(	ID_BranchTarget1),
	.i_IDRN_EXCCODE(		ID_EXCCODE1),
	
	.o_IDRN_valid(			RN_valid1),
	.o_IDRN_PC(				RN_PC1),
	.o_IDRN_Inst(			RN_Inst1),
	.o_IDRN_alcode(			RN_alcode1),
	.o_IDRN_bjcode(			RN_bjcode1),
	.o_IDRN_iecode(			RN_iecode1),
	.o_IDRN_mvcode(			RN_mvcode1),
	.o_IDRN_mdcode(			RN_mdcode1),
	.o_IDRN_memcode(		RN_memcode1),
	.o_IDRN_regsrc1(		RN_regsrc1),
	.o_IDRN_regsrc2(		RN_regsrc2),
	.o_IDRN_regdes(			RN_regdes1),
	.o_IDRN_BranchTaken(	RN_BranchTaken1),
	.o_IDRN_BranchTarget(	RN_BranchTarget1),
	.o_IDRN_EXCCODE(		RN_EXCCODE1)
	
);

IDRN CORE_IDRN2(
	
	.i_IDRN_clk(			RN_clk),
	.i_IDRN_enable(			RN_enable),
	.i_IDRN_reset(			RN_reset),
	
	.i_IDRN_valid(			ID_valid2),
	.i_IDRN_PC(				ID_PC2),
	.i_IDRN_Inst(			ID_Inst2),
	.i_IDRN_alcode(			ID_alcode2),
	.i_IDRN_bjcode(			ID_bjcode2),
	.i_IDRN_iecode(			ID_iecode2),
	.i_IDRN_mvcode(			ID_mvcode2),
	.i_IDRN_mdcode(			ID_mdcode2),
	.i_IDRN_memcode(		ID_memcode2),
	.i_IDRN_regsrc1(		ID_regsrc3),
	.i_IDRN_regsrc2(		ID_regsrc4),
	.i_IDRN_regdes(			ID_regdes2),
	.i_IDRN_BranchTaken(	ID_BranchTaken2),
	.i_IDRN_BranchTarget(	ID_BranchTarget2),
	.i_IDRN_EXCCODE(		ID_EXCCODE2),
	
	.o_IDRN_valid(			RN_valid2),
	.o_IDRN_PC(				RN_PC2),
	.o_IDRN_Inst(			RN_Inst2),
	.o_IDRN_alcode(			RN_alcode2),
	.o_IDRN_bjcode(			RN_bjcode2),
	.o_IDRN_iecode(			RN_iecode2),
	.o_IDRN_mvcode(			RN_mvcode2),
	.o_IDRN_mdcode(			RN_mdcode2),
	.o_IDRN_memcode(		RN_memcode2),
	.o_IDRN_regsrc1(		RN_regsrc3),
	.o_IDRN_regsrc2(		RN_regsrc4),
	.o_IDRN_regdes(			RN_regdes2),
	.o_IDRN_BranchTaken(	RN_BranchTaken2),
	.o_IDRN_BranchTarget(	RN_BranchTarget2),
	.o_IDRN_EXCCODE(		RN_EXCCODE2)
	
);
	
	
	wire	[5:0]	RN_pregsrc1;
	wire	[5:0]	RN_pregsrc2;
	wire	[5:0]	RN_pregsrc3;
	wire	[5:0]	RN_pregsrc4;
	wire	[5:0]	RN_pregdes1;
	wire	[5:0]	RN_pregdes2;
	wire	[5:0]	RN_ppregdes1;
	wire	[5:0]	RN_ppregdes2;
	//RegisterMapper
	wire	[35:0]	RM_RegExecouted;
	wire	[63:0]	RM_PhyRegState;
	
	wire			RM_CMTrollback;
	wire	[5:0]	RM_CMTregdes1;
	wire	[5:0]	RM_CMTregdes2;
	wire	[5:0]	RM_CMTpregdes1;
	wire	[5:0]	RM_CMTpregdes2;
	wire	[5:0]	RM_CMTppregdes1;
	wire	[5:0]	RM_CMTppregdes2;

RegisterMapper CORE_RM(
	
	.i_RM_clk(			RN_clk),
	.i_RM_enable(		RN_enable),
	.i_RM_reset(		RN_reset),
	//regsrc
	.i_RM_regsrc1(		RN_regsrc1),
	.i_RM_regsrc2(		RN_regsrc2),
	.i_RM_regsrc3(		RN_regsrc3),
	.i_RM_regsrc4(		RN_regsrc4),
	.o_RM_pregsrc1(		RN_pregsrc1),
	.o_RM_pregsrc2(		RN_pregsrc2),
	.o_RM_pregsrc3(		RN_pregsrc3),
	.o_RM_pregsrc4(		RN_pregsrc4),
	//regdes
	.i_RM_regdes1(		RN_regdes1),
	.i_RM_regdes2(		RN_regdes2),
	.o_RM_pregdes1(		RN_pregdes1),
	.o_RM_pregdes2(		RN_pregdes2),
	.o_RM_ppregdes1(	RN_ppregdes1),
	.o_RM_ppregdes2(	RN_ppregdes2),
	//RegExecouted
	.i_RM_RegExecouted(	RM_RegExecouted),
	//Full
	.o_RM_stall(		RM_stall),
	//PhyRegState
	.o_RM_PhyRegState(	RM_PhyRegState),
	//COMMIT
	.CMTrollback(		RM_CMTrollback),
	.CMTregdes1(		RM_CMTregdes1),
	.CMTregdes2(		RM_CMTregdes2),
	.CMTpregdes1(		RM_CMTpregdes1),
	.CMTpregdes2(		RM_CMTpregdes2),
	.CMTppregdes1(		RM_CMTppregdes1),
	.CMTppregdes2(		RM_CMTppregdes2)
	
);
	
	//ReOrderBuffer
	wire	[5:0]	ROB_ReOrderNum1;
	wire	[5:0]	ROB_ReOrderNum2;
	
	/*
		<--------Schedule_Issue-------->
	*/
	
	wire			SC_clk = i_CORE_clk;
	wire			SC_reset;
	wire			SC_enable;
	//Inst1
	wire			SC_valid1;
	wire	[31:0]	SC_PC1;
	wire	[31:0]	SC_Inst1;
	wire	[4:0]	SC_alcode1;
	wire	[4:0]	SC_bjcode1;
	wire	[4:0]	SC_iecode1;
	wire	[4:0]	SC_mvcode1;
	wire	[4:0]	SC_mdcode1;
	wire	[4:0]	SC_memcode1;
	wire	[5:0]	SC_pregsrc1;
	wire	[5:0]	SC_pregsrc2;
	wire	[4:0]	SC_regdes1;
	wire	[5:0]	SC_pregdes1;
	wire	[5:0]	SC_ppregdes1;
	wire	[1:0]	SC_BranchTaken1;
	wire	[31:0]	SC_BranchTarget1;
	wire	[5:0]	SC_ReOrderNum1;
	wire	[7:0]	SC_EXCCODE1;
	//Inst2
	wire			SC_valid2;
	wire	[31:0]	SC_PC2;
	wire	[31:0]	SC_Inst2;
	wire	[4:0]	SC_alcode2;
	wire	[4:0]	SC_bjcode2;
	wire	[4:0]	SC_iecode2;
	wire	[4:0]	SC_mvcode2;
	wire	[4:0]	SC_mdcode2;
	wire	[4:0]	SC_memcode2;
	wire	[5:0]	SC_pregsrc3;
	wire	[5:0]	SC_pregsrc4;
	wire	[4:0]	SC_regdes2;
	wire	[5:0]	SC_pregdes2;
	wire	[5:0]	SC_ppregdes2;
	wire	[1:0]	SC_BranchTaken2;
	wire	[31:0]	SC_BranchTarget2;
	wire	[5:0]	SC_ReOrderNum2;
	wire	[7:0]	SC_EXCCODE2;
	
RNSC CORE_RNSC1(
	
	.i_RNSC_clk(			SC_clk),
	.i_RNSC_enable(			SC_enable),
	.i_RNSC_reset(			SC_reset),
	
	.i_RNSC_valid(			RN_valid1),
	.i_RNSC_PC(				RN_PC1),
	.i_RNSC_Inst(			RN_Inst1),
	.i_RNSC_alcode(			RN_alcode1),
	.i_RNSC_bjcode(			RN_bjcode1),
	.i_RNSC_iecode(			RN_iecode1),
	.i_RNSC_mvcode(			RN_mvcode1),
	.i_RNSC_mdcode(			RN_mdcode1),
	.i_RNSC_memcode(		RN_memcode1),
	.i_RNSC_pregsrc1(		RN_pregsrc1),
	.i_RNSC_pregsrc2(		RN_pregsrc2),
	.i_RNSC_regdes(			RN_regdes1),
	.i_RNSC_pregdes(		RN_pregdes1),
	.i_RNSC_ppregdes(		RN_ppregdes1),
	.i_RNSC_BranchTaken(	RN_BranchTaken1),
	.i_RNSC_BranchTarget(	RN_BranchTarget1),
	.i_RNSC_ReOrderNum(		ROB_ReOrderNum1),
	.i_RNSC_EXCCODE(		RN_EXCCODE1),
	
	.o_RNSC_valid(			SC_valid1),
	.o_RNSC_PC(				SC_PC1),
	.o_RNSC_Inst(			SC_Inst1),
	.o_RNSC_alcode(			SC_alcode1),
	.o_RNSC_bjcode(			SC_bjcode1),
	.o_RNSC_iecode(			SC_iecode1),
	.o_RNSC_mvcode(			SC_mvcode1),
	.o_RNSC_mdcode(			SC_mdcode1),
	.o_RNSC_memcode(		SC_memcode1),
	.o_RNSC_pregsrc1(		SC_pregsrc1),
	.o_RNSC_pregsrc2(		SC_pregsrc2),
	.o_RNSC_regdes(			SC_regdes1),
	.o_RNSC_pregdes(		SC_pregdes1),
	.o_RNSC_ppregdes(		SC_ppregdes1),
	.o_RNSC_BranchTaken(	SC_BranchTaken1),
	.o_RNSC_BranchTarget(	SC_BranchTarget1),
	.o_RNSC_ReOrderNum(		SC_ReOrderNum1),
	.o_RNSC_EXCCODE(		SC_EXCCODE1)

);

RNSC CORE_RNSC2(
	
	.i_RNSC_clk(			SC_clk),
	.i_RNSC_enable(			SC_enable),
	.i_RNSC_reset(			SC_reset),
	
	.i_RNSC_valid(			RN_valid2),
	.i_RNSC_PC(				RN_PC2),
	.i_RNSC_Inst(			RN_Inst2),
	.i_RNSC_alcode(			RN_alcode2),
	.i_RNSC_bjcode(			RN_bjcode2),
	.i_RNSC_iecode(			RN_iecode2),
	.i_RNSC_mvcode(			RN_mvcode2),
	.i_RNSC_mdcode(			RN_mdcode2),
	.i_RNSC_memcode(		RN_memcode2),
	.i_RNSC_pregsrc1(		RN_pregsrc3),
	.i_RNSC_pregsrc2(		RN_pregsrc4),
	.i_RNSC_regdes(			RN_regdes2),
	.i_RNSC_pregdes(		RN_pregdes2),
	.i_RNSC_ppregdes(		RN_ppregdes2),
	.i_RNSC_BranchTaken(	RN_BranchTaken2),
	.i_RNSC_BranchTarget(	RN_BranchTarget2),
	.i_RNSC_ReOrderNum(		ROB_ReOrderNum2),
	.i_RNSC_EXCCODE(		RN_EXCCODE2),
	
	.o_RNSC_valid(			SC_valid2),
	.o_RNSC_PC(				SC_PC2),
	.o_RNSC_Inst(			SC_Inst2),
	.o_RNSC_alcode(			SC_alcode2),
	.o_RNSC_bjcode(			SC_bjcode2),
	.o_RNSC_iecode(			SC_iecode2),
	.o_RNSC_mvcode(			SC_mvcode2),
	.o_RNSC_mdcode(			SC_mdcode2),
	.o_RNSC_memcode(		SC_memcode2),
	.o_RNSC_pregsrc1(		SC_pregsrc3),
	.o_RNSC_pregsrc2(		SC_pregsrc4),
	.o_RNSC_regdes(			SC_regdes2),
	.o_RNSC_pregdes(		SC_pregdes2),
	.o_RNSC_ppregdes(		SC_ppregdes2),
	.o_RNSC_BranchTaken(	SC_BranchTaken2),
	.o_RNSC_BranchTarget(	SC_BranchTarget2),
	.o_RNSC_ReOrderNum(		SC_ReOrderNum2),
	.o_RNSC_EXCCODE(		SC_EXCCODE2)

);
	
	wire			IS_clk = i_CORE_clk;
	wire			IS_reset;
	wire			IS_enable;
	//Inst1
	wire	[64:0]	IS_VIP1;		//{[64]valid, [63:32]Inst, [31:0]PC}
	wire	[11:0]	IS_PRSrc1;	//{[11:6]pregsrc1, [5:0]pregsrc2}
	wire	[16:0]	IS_PRDes1;	//{[16:12]regdes, [11:6]pregdes, [5:0]ppregdes}
	wire	[29:0]	IS_IDcode1;	//{[29:25]alcode, [24:20]bjcode, [19:15]iecode, [14:10]mvcode, [9:5]mdcode, [4:0]memcode}			
	wire	[33:0]	IS_BranchTT1;	//{[33:32]BranchTaken, [31:0]BranchTarget}
	wire	[7:0]	IS_EXCCODE1;
	wire	[5:0]	IS_ReOrderNum1;
	//Inst2
	wire	[64:0]	IS_VIP2;
	wire	[11:0]	IS_PRSrc2;	
	wire	[16:0]	IS_PRDes2;	
	wire	[29:0]	IS_IDcode2;
	wire	[33:0]	IS_BranchTT2;
	wire	[7:0]	IS_EXCCODE2;
	wire	[5:0]	IS_ReOrderNum2;
	//IntQueue
	wire			IQ_CMTrollback;
	
IntQueue CORE_IQ(
	
	.i_IQ_clk(			IS_clk),
	.i_IQ_enable(		IS_enable),
	.i_IQ_reset(		IS_reset),
	//Stall
	.o_IQ_stall(		LSQ_stall),
	.i_IQ_IssueStall1(	1'd0),
	.i_IQ_IssueStall2(	1'd0),
	//PhyRegState
	.i_IQ_PhyRegState(	RM_PhyRegState),
	//EnQueue
	.i_IQ_VIP1(			{SC_valid1, SC_Inst1, SC_PC1}),	//{[64]valid, [63:32]Inst, [31:0]PC}
	.i_IQ_PRSrc1(		{SC_pregsrc1, SC_pregsrc2}),	//{[11:6]pregsrc1, [5:0]pregsrc2}
	.i_IQ_PRDes1(		{SC_regdes1, SC_pregdes1, SC_ppregdes1}),	//{[16:12]regdes, [11:6]pregdes, [5:0]ppregdes}
	.i_IQ_IDcode1(		{SC_alcode1, SC_bjcode1, SC_iecode1, SC_mvcode1, SC_mdcode1, SC_memcode1}),	//{[29:25]alcode, [24:20]bjcode, [19:15]iecode, [14:10]mvcode, [9:5]mdcode, [4:0]memcode}			
	.i_IQ_BranchTT1(	{SC_BranchTaken1, SC_BranchTarget1}),	//{[33:32]BranchTaken, [31:0]BranchTarget}
	.i_IQ_EXCCODE1(		SC_EXCCODE1),
	.i_IQ_ReOrderNum1(	SC_ReOrderNum1),
	.i_IQ_VIP2(			{SC_valid2, SC_Inst2, SC_PC2}),		
	.i_IQ_PRSrc2(		{SC_pregsrc3, SC_pregsrc4}),	
	.i_IQ_PRDes2(		{SC_regdes2, SC_pregdes2, SC_ppregdes2}),	
	.i_IQ_IDcode2(		{SC_alcode2, SC_bjcode2, SC_iecode2, SC_mvcode2, SC_mdcode2, SC_memcode2}),
	.i_IQ_BranchTT2(	{SC_BranchTaken2, SC_BranchTarget2}),
	.i_IQ_EXCCODE2(		SC_EXCCODE2),
	.i_IQ_ReOrderNum2(	SC_ReOrderNum2),
	//DeQueue
	.o_IQ_VIP1(			IS_VIP1),		//{[64]valid, [63:32]Inst, [31:0]PC}
	.o_IQ_PRSrc1(		IS_PRSrc1),	//{[11:6]pregsrc1, [5:0]pregsrc2}
	.o_IQ_PRDes1(		IS_PRDes1),	//{[16:12]regdes, [11:6]pregdes, [5:0]ppregdes}
	.o_IQ_IDcode1(		IS_IDcode1),	//{[29:25]alcode, [24:20]bjcode, [19:15]iecode, [14:10]mvcode, [9:5]mdcode, [4:0]memcode}			
	.o_IQ_BranchTT1(	IS_BranchTT1),	//{[33:32]BranchTaken, [31:0]BranchTarget}
	.o_IQ_EXCCODE1(		IS_EXCCODE1),
	.o_IQ_ReOrderNum1(	IS_ReOrderNum1),
	.o_IQ_VIP2(			IS_VIP2),
	.o_IQ_PRSrc2(		IS_PRSrc2),	
	.o_IQ_PRDes2(		IS_PRDes2),	
	.o_IQ_IDcode2(		IS_IDcode2),
	.o_IQ_BranchTT2(	IS_BranchTT2),
	.o_IQ_EXCCODE2(		IS_EXCCODE2),
	.o_IQ_ReOrderNum2(	IS_ReOrderNum2),
	//COMMIT
	.I_IQ_CMTrollback(	IQ_CMTrollback)

);

	//Inst3
	wire	[64:0]	IS_VIP3;
	wire	[11:0]	IS_PRSrc3;	
	wire	[16:0]	IS_PRDes3;	
	wire	[29:0]	IS_IDcode3;
	wire	[7:0]	IS_EXCCODE3;
	wire	[5:0]	IS_ReOrderNum3;
	//LoadStorQueue
	wire			LSQ_CMTrollback;

LoadStoreQueue CORE_LSQ(
	
	.i_LSQ_clk(			IS_clk),
	.i_LSQ_enable(		IS_enable),
	.i_LSQ_reset(		IS_reset),
	//Stall
	.o_LSQ_stall(		LSQ_stall),
	.i_LSQ_IssueStall(	Load_stall),
	//PhyRegState
	.i_LSQ_PhyRegState(	RM_PhyRegState),
	//EnQueue
	.i_LSQ_VIP1(		{SC_valid1, SC_Inst1, SC_PC1}),		//{[64]valid, [63:32]Inst, [31:0]PC}
	.i_LSQ_PRSrc1(		{SC_pregsrc1, SC_pregsrc2}),	//{[11:6]pregsrc1, [5:0]pregsrc2}
	.i_LSQ_PRDes1(		{SC_regdes1, SC_pregdes1, SC_ppregdes1}),	//{[16:12]regdes, [11:6]pregdes, [5:0]ppregdes}
	.i_LSQ_IDcode1(		{SC_alcode1, SC_bjcode1, SC_iecode1, SC_mvcode1, SC_mdcode1, SC_memcode1}),	//{[29:25]alcode, [24:20]bjcode, [19:15]iecode, [14:10]mvcode, [9:5]mdcode, [4:0]memcode}			
	.i_LSQ_EXCCODE1(	SC_EXCCODE1),
	.i_LSQ_ReOrderNum1(	SC_ReOrderNum1),
	.i_LSQ_VIP2(		{SC_valid2, SC_Inst2, SC_PC2}),		
	.i_LSQ_PRSrc2(		{SC_pregsrc3, SC_pregsrc4}),	
	.i_LSQ_PRDes2(		{SC_regdes2, SC_pregdes2, SC_ppregdes2}),	
	.i_LSQ_IDcode2(		{SC_alcode2, SC_bjcode2, SC_iecode2, SC_mvcode2, SC_mdcode2, SC_memcode2}),
	.i_LSQ_EXCCODE2(	SC_EXCCODE2),
	.i_LSQ_ReOrderNum2(	SC_ReOrderNum2),
	//DeQueue
	.o_LSQ_VIP(			IS_VIP3),		//{[64]valid, [63:32]Inst, [31:0]PC}
	.o_LSQ_PRSrc(		IS_PRSrc3),	//{[11:6]pregsrc1, [5:0]pregsrc2}
	.o_LSQ_PRDes(		IS_PRDes3),	//{[16:12]regdes, [11:6]pregdes, [5:0]ppregdes}
	.o_LSQ_IDcode(		IS_IDcode3),	//{[29:25]alcode, [24:20]bjcode, [19:15]iecode, [14:10]mvcode, [9:5]mdcode, [4:0]memcode}			
	.o_LSQ_EXCCODE(		IS_EXCCODE3),
	.o_LSQ_ReOrderNum(	IS_ReOrderNum3),
	//COMMIT
	.I_LSQ_CMTrollback(	LSQ_CMTrollback)

);
	
	//Inst4
	wire	[64:0]	IS_VIP4;
	wire	[11:0]	IS_PRSrc4;	
	wire	[16:0]	IS_PRDes4;	
	wire	[29:0]	IS_IDcode4;
	wire	[7:0]	IS_EXCCODE4;
	wire	[5:0]	IS_ReOrderNum4;
	//LoadStorQueue
	wire			MQ_CMTrollback;
	
MxQueue CORE_MQ(
	
	.i_MQ_clk(			IS_clk),
	.i_MQ_enable(		IS_enable),
	.i_MQ_reset(		IS_reset),
	//Stall
	.o_MQ_stall(		MQ_stall),
	.i_MQ_IssueStall(	MDU_stall),
	//PhyRegState
	.i_MQ_PhyRegState(	RM_PhyRegState),
	//EnQueue
	.i_MQ_VIP1(			{SC_valid1, SC_Inst1, SC_PC1}),	//{[64]valid, [63:32]Inst, [31:0]PC}
	.i_MQ_PRSrc1(		{SC_pregsrc1, SC_pregsrc2}),	//{[11:6]pregsrc1, [5:0]pregsrc2}
	.i_MQ_PRDes1(		{SC_regdes1, SC_pregdes1, SC_ppregdes1}),	//{[16:12]regdes, [11:6]pregdes, [5:0]ppregdes}
	.i_MQ_IDcode1(		{SC_alcode1, SC_bjcode1, SC_iecode1, SC_mvcode1, SC_mdcode1, SC_memcode1}),	//{[29:25]alcode, [24:20]bjcode, [19:15]iecode, [14:10]mvcode, [9:5]mdcode, [4:0]memcode}			
	.i_MQ_EXCCODE1(		SC_EXCCODE1),
	.i_MQ_ReOrderNum1(	SC_ReOrderNum1),
	.i_MQ_VIP2(			{SC_valid2, SC_Inst2, SC_PC2}),	
	.i_MQ_PRSrc2(		{SC_pregsrc3, SC_pregsrc4}),	
	.i_MQ_PRDes2(		{SC_regdes2, SC_pregdes2, SC_ppregdes2}),	
	.i_MQ_IDcode2(		{SC_alcode2, SC_bjcode2, SC_iecode2, SC_mvcode2, SC_mdcode2, SC_memcode2}),
	.i_MQ_EXCCODE2(		SC_EXCCODE2),
	.i_MQ_ReOrderNum2(	SC_ReOrderNum2),
	//DeQueue
	.o_MQ_VIP(			IS_VIP4),		//{[64]valid, [63:32]Inst, [31:0]PC}
	.o_MQ_PRSrc(		IS_PRSrc4),		//{[11:6]pregsrc1, [5:0]pregsrc2}
	.o_MQ_PRDes(		IS_PRDes4),		//{[16:12]regdes, [11:6]pregdes, [5:0]ppregdes}
	.o_MQ_IDcode(		IS_IDcode4),	//{[29:25]alcode, [24:20]bjcode, [19:15]iecode, [14:10]mvcode, [9:5]mdcode, [4:0]memcode}			
	.o_MQ_EXCCODE(		IS_EXCCODE4),
	.o_MQ_ReOrderNum(	IS_ReOrderNum4),
	//COMMIT
	.I_MQ_CMTrollback(	MQ_CMTrollback)

);

	/*
		<--------ReadRegisters-------->
	*/
	
	wire			RR_clk = i_CORE_clk;
	wire			RR_reset;
	wire			RR_enable;
	
	//Inst1
	wire			RR_valid1;
	wire	[31:0]	RR_PC1;
	wire	[31:0]	RR_Inst1;
	wire	[4:0]	RR_alcode1;
	wire	[4:0]	RR_bjcode1;
	wire	[4:0]	RR_iecode1;
	wire	[4:0]	RR_mvcode1;
	wire	[4:0]	RR_mdcode1;
	wire	[4:0]	RR_memcode1;
	wire	[5:0]	RR_pregsrc1;
	wire	[5:0]	RR_pregsrc2;
	wire	[4:0]	RR_regdes1;
	wire	[5:0]	RR_pregdes1;
	wire	[5:0]	RR_ppregdes1;
	wire	[1:0]	RR_BranchTaken1;
	wire	[31:0]	RR_BranchTarget1;
	wire	[5:0]	RR_ReOrderNum1;
	wire	[7:0]	RR_EXCCODE1;
	
ISRR CORE_ISRR1(
	
	.i_ISRR_clk(			RR_clk),
	.i_ISRR_enable(			RR_enable),
	.i_ISRR_reset(			RR_reset),
	
	.i_ISRR_valid(			IS_VIP1[64]),
	.i_ISRR_PC(				IS_VIP1[63:32]),
	.i_ISRR_Inst(			IS_VIP1[31:0]),
	.i_ISRR_alcode(			IS_IDcode1[29:25]),
	.i_ISRR_bjcode(			IS_IDcode1[24:20]),
	.i_ISRR_iecode(			IS_IDcode1[19:15]),
	.i_ISRR_mvcode(			IS_IDcode1[14:10]),
	.i_ISRR_mdcode(			IS_IDcode1[9:5]),
	.i_ISRR_memcode(		IS_IDcode1[4:0]),
	.i_ISRR_pregsrc1(		IS_PRSrc1[11:6]),
	.i_ISRR_pregsrc2(		IS_PRSrc1[5:0]),
	.i_ISRR_regdes(			IS_PRDes1[16:12]),
	.i_ISRR_pregdes(		IS_PRDes1[11:6]),
	.i_ISRR_ppregdes(		IS_PRDes1[5:0]),
	.i_ISRR_BranchTaken(	IS_BranchTT1[33:32]),
	.i_ISRR_BranchTarget(	IS_BranchTT1[31:0]),
	.i_ISRR_ReOrderNum(		IS_ReOrderNum1),
	.i_ISRR_EXCCODE(		IS_EXCCODE1),
	
	.o_ISRR_valid(			RR_valid1),
	.o_ISRR_PC(				RR_PC1),
	.o_ISRR_Inst(			RR_Inst1),
	.o_ISRR_alcode(			RR_alcode1),
	.o_ISRR_bjcode(			RR_bjcode1),
	.o_ISRR_iecode(			RR_iecode1),
	.o_ISRR_mvcode(			RR_mvcode1),
	.o_ISRR_mdcode(			RR_mdcode1),
	.o_ISRR_memcode(		RR_memcode1),
	.o_ISRR_pregsrc1(		RR_pregsrc1),
	.o_ISRR_pregsrc2(		RR_pregsrc2),
	.o_ISRR_regdes(			RR_regdes1),
	.o_ISRR_pregdes(		RR_pregdes1),
	.o_ISRR_ppregdes(		RR_ppregdes1),
	.o_ISRR_BranchTaken(	RR_BranchTaken1),
	.o_ISRR_BranchTarget(	RR_BranchTarget1),
	.o_ISRR_ReOrderNum(		RR_ReOrderNum1),
	.o_ISRR_EXCCODE(		RR_EXCCODE1)

);

	//Inst2
	wire			RR_valid2;
	wire	[31:0]	RR_PC2;
	wire	[31:0]	RR_Inst2;
	wire	[4:0]	RR_alcode2;
	wire	[4:0]	RR_bjcode2;
	wire	[4:0]	RR_iecode2;
	wire	[4:0]	RR_mvcode2;
	wire	[4:0]	RR_mdcode2;
	wire	[4:0]	RR_memcode2;
	wire	[5:0]	RR_pregsrc3;
	wire	[5:0]	RR_pregsrc4;
	wire	[4:0]	RR_regdes2;
	wire	[5:0]	RR_pregdes2;
	wire	[5:0]	RR_ppregdes2;
	wire	[1:0]	RR_BranchTaken2;
	wire	[31:0]	RR_BranchTarget2;
	wire	[5:0]	RR_ReOrderNum2;
	wire	[7:0]	RR_EXCCODE2;

ISRR CORE_ISRR2(
	
	.i_ISRR_clk(			RR_clk),
	.i_ISRR_enable(			RR_enable),
	.i_ISRR_reset(			RR_reset),
	
	.i_ISRR_valid(			IS_VIP2[64]),
	.i_ISRR_PC(				IS_VIP2[63:32]),
	.i_ISRR_Inst(			IS_VIP2[31:0]),
	.i_ISRR_alcode(			IS_IDcode2[29:25]),
	.i_ISRR_bjcode(			IS_IDcode2[24:20]),
	.i_ISRR_iecode(			IS_IDcode2[19:15]),
	.i_ISRR_mvcode(			IS_IDcode2[14:10]),
	.i_ISRR_mdcode(			IS_IDcode2[9:5]),
	.i_ISRR_memcode(		IS_IDcode2[4:0]),
	.i_ISRR_pregsrc1(		IS_PRSrc2[11:6]),
	.i_ISRR_pregsrc2(		IS_PRSrc2[5:0]),
	.i_ISRR_regdes(			IS_PRDes2[16:12]),
	.i_ISRR_pregdes(		IS_PRDes2[11:6]),
	.i_ISRR_ppregdes(		IS_PRDes2[5:0]),
	.i_ISRR_BranchTaken(	IS_BranchTT2[33:32]),
	.i_ISRR_BranchTarget(	IS_BranchTT2[31:0]),
	.i_ISRR_ReOrderNum(		IS_ReOrderNum2),
	.i_ISRR_EXCCODE(		IS_EXCCODE2),
	
	.o_ISRR_valid(			RR_valid2),
	.o_ISRR_PC(				RR_PC2),
	.o_ISRR_Inst(			RR_Inst2),
	.o_ISRR_alcode(			RR_alcode2),
	.o_ISRR_bjcode(			RR_bjcode2),
	.o_ISRR_iecode(			RR_iecode2),
	.o_ISRR_mvcode(			RR_mvcode2),
	.o_ISRR_mdcode(			RR_mdcode2),
	.o_ISRR_memcode(		RR_memcode2),
	.o_ISRR_pregsrc1(		RR_pregsrc3),
	.o_ISRR_pregsrc2(		RR_pregsrc4),
	.o_ISRR_regdes(			RR_regdes2),
	.o_ISRR_pregdes(		RR_pregdes2),
	.o_ISRR_ppregdes(		RR_ppregdes2),
	.o_ISRR_BranchTaken(	RR_BranchTaken2),
	.o_ISRR_BranchTarget(	RR_BranchTarget2),
	.o_ISRR_ReOrderNum(		RR_ReOrderNum2),
	.o_ISRR_EXCCODE(		RR_EXCCODE2)

);
	
	//Inst3
	wire			RR_valid3;
	wire	[31:0]	RR_PC3;
	wire	[31:0]	RR_Inst3;
	wire	[4:0]	RR_alcode3;
	wire	[4:0]	RR_bjcode3;
	wire	[4:0]	RR_iecode3;
	wire	[4:0]	RR_mvcode3;
	wire	[4:0]	RR_mdcode3;
	wire	[4:0]	RR_memcode3;
	wire	[5:0]	RR_pregsrc5;
	wire	[5:0]	RR_pregsrc6;
	wire	[4:0]	RR_regdes3;
	wire	[5:0]	RR_pregdes3;
	wire	[5:0]	RR_ppregdes3;
	wire	[5:0]	RR_ReOrderNum3;
	wire	[7:0]	RR_EXCCODE3;
	
ISRR CORE_ISRR3(
	
	.i_ISRR_clk(			RR_clk),
	.i_ISRR_enable(			RR_enable),
	.i_ISRR_reset(			RR_reset),
	
	.i_ISRR_valid(			IS_VIP3[64]),
	.i_ISRR_PC(				IS_VIP3[63:32]),
	.i_ISRR_Inst(			IS_VIP3[31:0]),
	.i_ISRR_alcode(			IS_IDcode3[29:25]),
	.i_ISRR_bjcode(			IS_IDcode3[24:20]),
	.i_ISRR_iecode(			IS_IDcode3[19:15]),
	.i_ISRR_mvcode(			IS_IDcode3[14:10]),
	.i_ISRR_mdcode(			IS_IDcode3[9:5]),
	.i_ISRR_memcode(		IS_IDcode3[4:0]),
	.i_ISRR_pregsrc1(		IS_PRSrc3[11:6]),
	.i_ISRR_pregsrc2(		IS_PRSrc3[5:0]),
	.i_ISRR_regdes(			IS_PRDes3[16:12]),
	.i_ISRR_pregdes(		IS_PRDes3[11:6]),
	.i_ISRR_ppregdes(		IS_PRDes3[5:0]),
	.i_ISRR_BranchTaken(	2'd0),
	.i_ISRR_BranchTarget(	32'd0),
	.i_ISRR_ReOrderNum(		IS_ReOrderNum3),
	.i_ISRR_EXCCODE(		IS_EXCCODE3),
	
	.o_ISRR_valid(			RR_valid3),
	.o_ISRR_PC(				RR_PC3),
	.o_ISRR_Inst(			RR_Inst3),
	.o_ISRR_alcode(			RR_alcode3),
	.o_ISRR_bjcode(			RR_bjcode3),
	.o_ISRR_iecode(			RR_iecode3),
	.o_ISRR_mvcode(			RR_mvcode3),
	.o_ISRR_mdcode(			RR_mdcode3),
	.o_ISRR_memcode(		RR_memcode3),
	.o_ISRR_pregsrc1(		RR_pregsrc3),
	.o_ISRR_pregsrc2(		RR_pregsrc3),
	.o_ISRR_regdes(			RR_regdes3),
	.o_ISRR_pregdes(		RR_pregdes3),
	.o_ISRR_ppregdes(		RR_ppregdes3),
	.o_ISRR_BranchTaken(	RR_BranchTaken3),
	.o_ISRR_BranchTarget(	RR_BranchTarget3),
	.o_ISRR_ReOrderNum(		RR_ReOrderNum3),
	.o_ISRR_EXCCODE(		RR_EXCCODE3)

);

	//Inst4
	wire			RR_valid4;
	wire	[31:0]	RR_PC4;
	wire	[31:0]	RR_Inst4;
	wire	[4:0]	RR_alcode4;
	wire	[4:0]	RR_bjcode4;
	wire	[4:0]	RR_iecode4;
	wire	[4:0]	RR_mvcode4;
	wire	[4:0]	RR_mdcode4;
	wire	[4:0]	RR_memcode4;
	wire	[5:0]	RR_pregsrc7;
	wire	[5:0]	RR_pregsrc8;
	wire	[4:0]	RR_regdes4;
	wire	[5:0]	RR_pregdes4;
	wire	[5:0]	RR_ppregdes4;
	wire	[5:0]	RR_ReOrderNum4;
	wire	[7:0]	RR_EXCCODE4;
	
ISRR CORE_ISRR4(
	
	.i_ISRR_clk(			RR_clk),
	.i_ISRR_enable(			RR_enable),
	.i_ISRR_reset(			RR_reset),
	
	.i_ISRR_valid(			IS_VIP4[64]),
	.i_ISRR_PC(				IS_VIP4[63:32]),
	.i_ISRR_Inst(			IS_VIP4[31:0]),
	.i_ISRR_alcode(			IS_IDcode4[29:25]),
	.i_ISRR_bjcode(			IS_IDcode4[24:20]),
	.i_ISRR_iecode(			IS_IDcode4[19:15]),
	.i_ISRR_mvcode(			IS_IDcode4[14:10]),
	.i_ISRR_mdcode(			IS_IDcode4[9:5]),
	.i_ISRR_memcode(		IS_IDcode4[4:0]),
	.i_ISRR_pregsrc1(		IS_PRSrc4[11:6]),
	.i_ISRR_pregsrc2(		IS_PRSrc4[5:0]),
	.i_ISRR_regdes(			IS_PRDes4[16:12]),
	.i_ISRR_pregdes(		IS_PRDes4[11:6]),
	.i_ISRR_ppregdes(		IS_PRDes4[5:0]),
	.i_ISRR_BranchTaken(	2'd0),
	.i_ISRR_BranchTarget(	32'd0),
	.i_ISRR_ReOrderNum(		IS_ReOrderNum4),
	.i_ISRR_EXCCODE(		IS_EXCCODE4),
	
	.o_ISRR_valid(			RR_valid4),
	.o_ISRR_PC(				RR_PC4),
	.o_ISRR_Inst(			RR_Inst4),
	.o_ISRR_alcode(			RR_alcode4),
	.o_ISRR_bjcode(			RR_bjcode4),
	.o_ISRR_iecode(			RR_iecode4),
	.o_ISRR_mvcode(			RR_mvcode4),
	.o_ISRR_mdcode(			RR_mdcode4),
	.o_ISRR_memcode(		RR_memcode4),
	.o_ISRR_pregsrc1(		RR_pregsrc4),
	.o_ISRR_pregsrc2(		RR_pregsrc4),
	.o_ISRR_regdes(			RR_regdes4),
	.o_ISRR_pregdes(		RR_pregdes4),
	.o_ISRR_ppregdes(		RR_ppregdes4),
	.o_ISRR_BranchTaken(	RR_BranchTaken4),
	.o_ISRR_BranchTarget(	RR_BranchTarget4),
	.o_ISRR_ReOrderNum(		RR_ReOrderNum4),
	.o_ISRR_EXCCODE(		RR_EXCCODE4)

);

	wire	[31:0]	RR_rdata1;
	wire	[31:0]	RR_rdata2;
	wire	[31:0]	RR_rdata3;
	wire	[31:0]	RR_rdata4;
	
	wire	[31:0]	RR_rdata5;
	wire	[31:0]	RR_rdata6;
	
	wire	[31:0]	RR_rdata7;
	wire	[31:0]	RR_rdata8;
	wire	[31:0]	RR_rdata9;
	
	//MFHI MFLO MFC0
	wire	[31:0]	RR_MFC0data;
	wire	[31:0]	RR_MFHIdata;
	wire	[31:0]	RR_MFLOdata;

PhysicalRegisterFile CORE_PRF(

	.i_PRF_clk(		RR_clk),
	.i_PRF_enable(	RR_enable),
	.i_PRF_reset(	RR_reset),
	//read
	.i_PRF_raddr1(	RR_pregsrc1),
	.i_PRF_raddr2(	RR_pregsrc2),
	.i_PRF_raddr3(	RR_pregsrc3),
	.i_PRF_raddr4(	RR_pregsrc4),
	
	.i_PRF_raddr5(	RR_pregsrc5),
	.i_PRF_raddr6(	RR_pregsrc6),
	
	.i_PRF_raddr7(	RR_pregsrc7),
	.i_PRF_raddr8(	RR_pregsrc8),
	.i_PRF_raddr9(	RR_ppregdes4),
	.i_PRF_raddr10(	6'd0),
	
	.o_PRF_rdata1(	RR_rdata1),
	.o_PRF_rdata2(	RR_rdata2),
	.o_PRF_rdata3(	RR_rdata3),
	.o_PRF_rdata4(	RR_rdata4),
	
	.o_PRF_rdata5(	RR_rdata5),
	.o_PRF_rdata6(	RR_rdata6),
	
	.o_PRF_rdata7(	RR_rdata7),
	.o_PRF_rdata8(	RR_rdata8),
	.o_PRF_rdata9(	RR_rdata9),
	.o_PRF_rdata10(	),
	//write
	.i_PRF_waddr1(	((RR_bjcode1!=5'd0)?RR_pregdes1:5'd0)),
	.i_PRF_waddr2(	((RR_bjcode2!=5'd0)?RR_pregdes2:5'd0)),
	.i_PRF_waddr3(	((RR_mvcode4==5'd1 || RR_mvcode4==5'd2 || RR_mvcode4==5'd5)?RR_pregdes4:5'd0)),
	.i_PRF_waddr4(	),
	.i_PRF_waddr5(	),
	.i_PRF_waddr6(	),
	.i_PRF_waddr7(	),
	.i_PRF_wdata1(	RR_PC1 + 32'd8),
	.i_PRF_wdata2(	RR_PC2 + 32'd8),
	.i_PRF_wdata3(	RR_MFC0data | RR_MFHIdata | RR_MFLOdata),
	.i_PRF_wdata4(	),
	.i_PRF_wdata5(	),
	.i_PRF_wdata6(	),
	.i_PRF_wdata7(	),
	
);

	//RRFinished1
	wire	RR_take1_BEQ	= (RR_bjcode1==5'd1) ? (RR_rdata1==RR_rdata2) : 1'd0;
	wire	RR_take1_BGTZ	= (RR_bjcode1==5'd2) ? (RR_rdata1>RR_rdata2) : 1'd0;
	wire	RR_take1_BGEZ	= (RR_bjcode1==5'd3) ? (RR_rdata1>=RR_rdata2) : 1'd0;
	wire	RR_take1_BGEZAL	= (RR_bjcode1==5'd4) ? (RR_rdata1>=RR_rdata2) : 1'd0;
	wire	RR_take1_BNE	= (RR_bjcode1==5'd5) ? (RR_rdata1!=RR_rdata2) : 1'd0;
	wire	RR_take1_BLEZ	= (RR_bjcode1==5'd6) ? (RR_rdata1<=RR_rdata2) : 1'd0;
	wire	RR_take1_BLTZ	= (RR_bjcode1==5'd7) ? (RR_rdata1<RR_rdata2) : 1'd0;
	wire	RR_take1_BLTZAL	= (RR_bjcode1==5'd8) ? (RR_rdata1<RR_rdata2) : 1'd0;
	wire	RR_take1_J		= (RR_bjcode1==5'd17);
	wire	RR_take1_JAL	= (RR_bjcode1==5'd18);
	wire	RR_take1_JR		= (RR_bjcode1==5'd19);
	wire	RR_take1_JALR	= (RR_bjcode1==5'd20);
	wire	RR_take1 = RR_take1_BEQ|RR_take1_BGTZ|RR_take1_BGEZ|RR_take1_BGEZAL|
					   RR_take1_BNE|RR_take1_BLEZ|RR_take1_BLTZ|RR_take1_BLTZAL|
					   RR_take1_J  |RR_take1_JAL |RR_take1_JR  |RR_take1_JALR;
	
	wire	RR_PC1_4 = RR_PC1 + 32'd0;
	wire	RR_target1_B = (RR_bjcode1==5'd1 || RR_bjcode1==5'd2 || RR_bjcode1==5'd3 || RR_bjcode1==5'd4 ||
							RR_bjcode1==5'd5 || RR_bjcode1==5'd6 || RR_bjcode1==5'd7 || RR_bjcode1==5'd8) ?
							({14{RR_Inst1[15]}, RR_Inst1[15:0], 2'd0} + RR_PC1_4) : 32'd0;
	wire	RR_target1_J = (RR_bjcode1==5'd17 || RR_bjcode1==5'd18) ? 
							{RR_PC1_4[31:28], RR_Inst1[25:0], 2'd0} : 32'd0;
	wire	RR_target1_R = (RR_bjcode1==5'd19 || RR_bjcode1==5'd20) ?
							RR_rdata1 : 32'd0;
	wire	RR_target1 = RR_target1_B | RR_target1_J | RR_target1_R;
	
	wire			RRFinished1		= (RR_bjcode1!=5'd0) || (RR_iecode1!=5'd0);
	wire	[16:0]	RRF1_PRDes		= {RR_regdes1, RR_pregdes1, RR_ppregdes1};
	wire	[31:0]	RRF1_RFwbdata	= RR_PC1 + 32'd8;
	wire	[34:0]	RRF1_BRANCH		= {RR_BranchTaken1[1], RR_take1, RR_target1}
	wire	[7:0]	RRF1_EXCCODE	= RR_EXCCODE1;
	wire	[5:0]	RRF1_ReOrderNum	= RR_ReOrderNum1;
	
	//RRFinished2
	wire	RR_take2_BEQ	= (RR_bjcode2==5'd1) ? (RR_rdata3==RR_rdata4) : 1'd0;
	wire	RR_take2_BGTZ	= (RR_bjcode2==5'd2) ? (RR_rdata3>RR_rdata4) : 1'd0;
	wire	RR_take2_BGEZ	= (RR_bjcode2==5'd3) ? (RR_rdata3>=RR_rdata4) : 1'd0;
	wire	RR_take2_BGEZAL	= (RR_bjcode2==5'd4) ? (RR_rdata3>=RR_rdata4) : 1'd0;
	wire	RR_take2_BNE	= (RR_bjcode2==5'd5) ? (RR_rdata3!=RR_rdata4) : 1'd0;
	wire	RR_take2_BLEZ	= (RR_bjcode2==5'd6) ? (RR_rdata3<=RR_rdata4) : 1'd0;
	wire	RR_take2_BLTZ	= (RR_bjcode2==5'd7) ? (RR_rdata3<RR_rdata4) : 1'd0;
	wire	RR_take2_BLTZAL	= (RR_bjcode2==5'd8) ? (RR_rdata3<RR_rdata4) : 1'd0;
	wire	RR_take2_J		= (RR_bjcode2==5'd17);
	wire	RR_take2_JAL	= (RR_bjcode2==5'd18);
	wire	RR_take2_JR		= (RR_bjcode2==5'd19);
	wire	RR_take2_JALR	= (RR_bjcode2==5'd20);
	wire	RR_take2 = RR_take2_BEQ|RR_take2_BGTZ|RR_take2_BGEZ|RR_take2_BGEZAL|
					   RR_take2_BNE|RR_take2_BLEZ|RR_take2_BLTZ|RR_take2_BLTZAL|
					   RR_take2_J  |RR_take2_JAL |RR_take2_JR  |RR_take2_JALR;
	
	wire	RR_PC2_4 = RR_PC2 + 32'd0;
	wire	RR_target2_B = (RR_bjcode2==5'd1 || RR_bjcode2==5'd2 || RR_bjcode2==5'd3 || RR_bjcode2==5'd4 ||
							RR_bjcode2==5'd5 || RR_bjcode2==5'd6 || RR_bjcode2==5'd7 || RR_bjcode2==5'd8) ?
							({14{RR_Inst2[15]}, RR_Inst2[15:0], 2'd0} + RR_PC2_4) : 32'd0;
	wire	RR_target2_J = (RR_bjcode2==5'd17 || RR_bjcode2==5'd18) ? 
							{RR_PC2_4[31:28], RR_Inst2[25:0], 2'd0} : 32'd0;
	wire	RR_target2_R = (RR_bjcode2==5'd19 || RR_bjcode2==5'd20) ?
							RR_rdata3 : 32'd0;
	wire	RR_target2 = RR_target2_B | RR_target2_J | RR_target2_R;
	
	wire			RRFinished2		= (RR_bjcode2!=5'd0) || (RR_iecode2!=5'd0);
	wire	[16:0]	RRF2_PRDes		= {RR_regdes2, RR_pregdes2, RR_ppregdes2};
	wire	[31:0]	RRF2_RFwbdata	= RR_PC2 + 32'd8;
	wire	[34:0]	RRF2_BRANCH		= {RR_BranchTaken2[1], RR_take2, RR_target2}
	wire	[7:0]	RRF2_EXCCODE	= RR_EXCCODE2;
	wire	[5:0]	RRF2_ReOrderNum	= RR_ReOrderNum2;
	
	wire	[31:0]	RR_CP0rdata;
	wire	[63:0]	RR_HILOdata;
	assign	RR_MFC0data = (RR_mvcode4==5'd5)?RR_CP0rdata:32'd0;
	assign	RR_MFHIdata = (RR_mvcode4==5'd1)?RR_HILOdata[63:32]:32'd0;
	assign	RR_MFLOdata = (RR_mvcode4==5'd2)?RR_HILOdata[31:0]:32'd0;
	
CoProcessor0 CORE_CP0(
	
	.i_CP0_clk(			RR_clk),
	.i_CP0_enable(		RR_enable),
	.i_CP0_reset(		RR_reset),
	//read
	.i_CP0_raddr(		RR_Inst4[15:11]),
	.i_CP0_rsel(		RR_Inst4[2:0]),
	.o_CP0_rdata(		RR_MFC0data),
	//write
	.i_CP0_wen(			),
	.i_CP0_waddr(		),
	.i_CP0_wsel(		),
	.i_CP0_wdata(		),
	//CP0data
	.o_CP0_Status(		RR_Status),
	.o_CP0_EPC(			RR_EPC),
	//COMMIT
	.i_CP0_CMTvaild(	),
	.i_CP0_CMTEXCCODE(	),
	.i_CP0_CMTPC(		),
	.i_CP0_CMTBadVAddr(	),
	
	.i_CP0_interrupt(	i_CORE_interrupt),
	.o_CP0_interrupt(	RR_interrupt)
);
	
HILO CORE_HILO(
	
	.i_HILO_clk(			RR_clk),
	.i_HILO_enable(			RR_enable),
	.i_HILO_reset(			RR_reset),
	
	.i_HILO_HILOwen(		{RR_mvcode4==5'd3, RR_mvcode4==5'd4}),
	.i_HILO_HILOwdata(		{RR_rdata7, RR_rdata7}),
	
	.o_HILO_HILOdata(		RR_HILOdata),
	//COMMIT
	.i_HILO_CMTrollback(	),
	.i_HILO_CMTHILOwen(		),
	.i_HILO_CMTHILOwdata(	)
	
);

	//RRFinished3
	wire			i_ROB_RRFinished3		= (RR_mvcode4!=5'd0);
	wire	[16:0]	i_ROB_RRF3_PRDes		= {RR_regdes3, RR_pregdes3, RR_ppregdes3}
	wire	[31:0]	i_ROB_RRF3_RFwbdata		= RR_MFC0data | RR_MFHIdata | RR_MFLOdata;
	wire	[65:0]	i_ROB_RRF3_CP0w			= {RR_mvcode4==5'd6, RR_Inst4[15:11], RR_Inst4[2:0], RR_rdata8};
	wire	[40:0]	i_ROB_RRF3_HILOw		= {RR_mvcode4==5'd3, RR_mvcode4==5'd4, RR_rdata7, RR_rdata7};
	wire	[5:0]	i_ROB_RRF3_ReOrderNum	= RR_ReOrderNum4;
	

	/*
		<--------Executed-------->
	*/
	
	wire			EX_clk = i_CORE_clk;
	wire			EX_reset;
	wire			EX_enable;
	//Inst1
	wire			EX_valid1;
	wire	[31:0]	EX_PC1;
	wire	[31:0]	EX_Inst1;
	wire	[4:0]	EX_alcode1;
	wire	[4:0]	EX_mdcode1;
	wire	[4:0]	EX_memcode1;
	wire	[31:0]	EX_operand1;
	wire	[31:0]	EX_operand2;
	wire	[63:0]	EX_HILOdata1;
	wire	[4:0]	EX_regdes1;
	wire	[5:0]	EX_pregdes1;
	wire	[5:0]	EX_ppregdes1;
	wire	[5:0]	EX_ReOrderNum1;
	wire	[7:0]	EX_EXCCODE1;
	
RREX CORE_RREX1(

	.i_RREX_clk(		EX_clk),
	.i_RREX_enable(		EX_enable),
	.i_RREX_reset(		EX_reset),
	
	.i_RREX_valid(		RR_valid1),
	.i_RREX_PC(			RR_PC1),
	.i_RREX_Inst(		RR_Inst1),
	.i_RREX_alcode(		RR_alcode1),
	.i_RREX_mdcode(		RR_mdcode1),
	.i_RREX_memcode(	RR_memcode1),
	.i_RREX_operand1(	RR_rdata1),
	.i_RREX_operand2(	RR_rdata2),
	.i_RREX_HILOdata(	RR_HILOdata),
	.i_RREX_regdes(		RR_regdes1),
	.i_RREX_pregdes(	RR_pregdes1),
	.i_RREX_ppregdes(	RR_ppregdes1),
	.i_RREX_ReOrderNum(	RR_ReOrderNum1),
	.i_RREX_EXCCODE(	RR_EXCCODE1),
	
	.o_RREX_valid(		EX_valid1),
	.o_RREX_PC(			EX_PC1),
	.o_RREX_Inst(		EX_Inst1),
	.o_RREX_alcode(		EX_alcode1),
	.o_RREX_mdcode(		EX_mdcode1),
	.o_RREX_memcode(	EX_memcode1),
	.o_RREX_operand1(	EX_operand1),
	.o_RREX_operand2(	EX_operand2),
	.o_RREX_HILOdata(	EX_HILOdata1),
	.o_RREX_regdes(		EX_regdes1),
	.o_RREX_pregdes(	EX_pregdes1),
	.o_RREX_ppregdes(	EX_ppregdes1),
	.o_RREX_ReOrderNum(	EX_ReOrderNum1),
	.o_RREX_EXCCODE(	EX_EXCCODE1)

);
	
	wire	[31:0]	EX_ALU1result;
	wire			EX_ALU1ov;
	
ArithmeticLogicunit CORE_ALU1(

	.i_ALU_alcode(		EX_alcode1),
	.i_ALU_operand1(	EX_operand1),
	.i_ALU_operand2(	EX_operand2),
	.i_ALU_imm(			EX_Inst1[15:0]),
	
	.o_ALU_result(		EX_ALU1result),
	.o_ALU_overflow(	EX_ALU1ov)

);

	//EXFinished4
	wire			EXFinished4		= EX_valid1;
	wire	[16:0]	EXF4_PRDes		= {EX_regdes1, EX_pregdes1, EX_ppregdes1};
	wire	[31:0]	EXF4_RFwbdata	= EX_ALU1result;
	wire	[7:0]	EXF4_EXCCODE	= {EX_ALU1ov, EX_EXCCODE1[6:0], 5'd12};
	wire	[5:0]	EXF4_ReOrderNum	= EX_ReOrderNum1;
	
	//Inst2
	wire			EX_valid2;
	wire	[31:0]	EX_PC2;
	wire	[31:0]	EX_Inst2;
	wire	[4:0]	EX_alcode2;
	wire	[4:0]	EX_mdcode2;
	wire	[4:0]	EX_memcode2;
	wire	[31:0]	EX_operand3;
	wire	[31:0]	EX_operand4;
	wire	[63:0]	EX_HILOdata2;
	wire	[4:0]	EX_regdes2;
	wire	[5:0]	EX_pregdes2;
	wire	[5:0]	EX_ppregdes2;
	wire	[5:0]	EX_ReOrderNum2;
	wire	[7:0]	EX_EXCCODE2;
	
RREX CORE_RREX2(

	.i_RREX_clk(		EX_clk),
	.i_RREX_enable(		EX_enable),
	.i_RREX_reset(		EX_reset),
	
	.i_RREX_valid(		RR_valid2),
	.i_RREX_PC(			RR_PC2),
	.i_RREX_Inst(		RR_Inst2),
	.i_RREX_alcode(		RR_alcode2),
	.i_RREX_mdcode(		RR_mdcode2),
	.i_RREX_memcode(	RR_memcode2),
	.i_RREX_operand1(	RR_rdata3),
	.i_RREX_operand2(	RR_rdata4),
	.i_RREX_HILOdata(	RR_HILOdata),
	.i_RREX_regdes(		RR_regdes2),
	.i_RREX_pregdes(	RR_pregdes2),
	.i_RREX_ppregdes(	RR_ppregdes2),
	.i_RREX_ReOrderNum(	RR_ReOrderNum2),
	.i_RREX_EXCCODE(	RR_EXCCODE2),
	
	.o_RREX_valid(		EX_valid2),
	.o_RREX_PC(			EX_PC2),
	.o_RREX_Inst(		EX_Inst2),
	.o_RREX_alcode(		EX_alcode2),
	.o_RREX_mdcode(		EX_mdcode2),
	.o_RREX_memcode(	EX_memcode2),
	.o_RREX_operand1(	EX_operand3),
	.o_RREX_operand2(	EX_operand4),
	.o_RREX_HILOdata(	EX_HILOdata2),
	.o_RREX_regdes(		EX_regdes2),
	.o_RREX_pregdes(	EX_pregdes2),
	.o_RREX_ppregdes(	EX_ppregdes2),
	.o_RREX_ReOrderNum(	EX_ReOrderNum2),
	.o_RREX_EXCCODE(	EX_EXCCODE2)

);

	wire	[31:0]	EX_ALU2result;
	wire			EX_ALU2ov;
	
ArithmeticLogicunit CORE_ALU2(

	.i_ALU_alcode(		EX_alcode2),
	.i_ALU_operand1(	EX_operand3),
	.i_ALU_operand2(	EX_operand4),
	.i_ALU_imm(			EX_Inst2[15:0]),
	
	.o_ALU_result(		EX_ALU2result),
	.o_ALU_overflow(	EX_ALU2ov)

);

	//EXFinished4
	wire			EXFinished5		= EX_valid2;
	wire	[16:0]	EXF5_PRDes		= {EX_regdes2, EX_pregdes2, EX_ppregdes2};
	wire	[31:0]	EXF5_RFwbdata	= EX_ALU2result;
	wire	[7:0]	EXF5_EXCCODE	= {EX_ALU2ov, EX_EXCCODE1[6:0], 5'd12};
	wire	[5:0]	EXF5_ReOrderNum	= EX_ReOrderNum2;
	
	//Inst3
	wire			EX_valid3;
	wire	[31:0]	EX_PC3;
	wire	[31:0]	EX_Inst3;
	wire	[4:0]	EX_alcode3;
	wire	[4:0]	EX_mdcode3;
	wire	[4:0]	EX_memcode3;
	wire	[31:0]	EX_operand5;
	wire	[31:0]	EX_operand6;
	wire	[63:0]	EX_HILOdata3;
	wire	[4:0]	EX_regdes3;
	wire	[5:0]	EX_pregdes3;
	wire	[5:0]	EX_ppregdes3;
	wire	[5:0]	EX_ReOrderNum3;
	wire	[7:0]	EX_EXCCODE3;
	
RREX CORE_RREX3(

	.i_RREX_clk(		EX_clk),
	.i_RREX_enable(		EX_enable),
	.i_RREX_reset(		EX_reset),
	
	.i_RREX_valid(		RR_valid3),
	.i_RREX_PC(			RR_PC3),
	.i_RREX_Inst(		RR_Inst3),
	.i_RREX_alcode(		RR_alcode3),
	.i_RREX_mdcode(		RR_mdcode3),
	.i_RREX_memcode(	RR_memcode3),
	.i_RREX_operand1(	RR_rdata5),
	.i_RREX_operand2(	RR_rdata6),
	.i_RREX_HILOdata(	RR_HILOdata),
	.i_RREX_regdes(		RR_regdes3),
	.i_RREX_pregdes(	RR_pregdes3),
	.i_RREX_ppregdes(	RR_ppregdes3),
	.i_RREX_ReOrderNum(	RR_ReOrderNum3),
	.i_RREX_EXCCODE(	RR_EXCCODE3),
	
	.o_RREX_valid(		EX_valid3),
	.o_RREX_PC(			EX_PC3),
	.o_RREX_Inst(		EX_Inst3),
	.o_RREX_alcode(		EX_alcode3),
	.o_RREX_mdcode(		EX_mdcode3),
	.o_RREX_memcode(	EX_memcode3),
	.o_RREX_operand1(	EX_operand5),
	.o_RREX_operand2(	EX_operand6),
	.o_RREX_HILOdata(	EX_HILOdata3),
	.o_RREX_regdes(		EX_regdes3),
	.o_RREX_pregdes(	EX_pregdes3),
	.o_RREX_ppregdes(	EX_ppregdes3),
	.o_RREX_ReOrderNum(	EX_ReOrderNum3),
	.o_RREX_EXCCODE(	EX_EXCCODE3)

);
	
	wire	[31:0]	EX_AGUVaddr;
	wire	[2:0]	EX_AGUSize;
	
AddressGenerationUnit CORE_AGU(

	.i_AGU_memcode(	EX_memcode3),
	.i_AGU_base(	EX_operand5),
	.i_AGU_imm(		EX_Inst3[15:0]),
	
	.o_AGU_Vaddr(	EX_AGUVaddr),
	.o_AGU_Size(	EX_AGUSize)

);
	
	wire	LW	= (i_AGU_memcode == 5'd1);
	wire	LH	= (i_AGU_memcode == 5'd9);
	wire	LHU	= (i_AGU_memcode == 5'd11);
	wire	LB	= (i_AGU_memcode == 5'd10);
	wire	LBU	= (i_AGU_memcode == 5'd12);
	wire	SW	= (i_AGU_memcode == 5'd5);
	wire	SH	= (i_AGU_memcode == 5'd13);
	wire	SB	= (i_AGU_memcode == 5'd14);
	
	wire	AdEL = 	LW && (EX_AGUVaddr[1:0]!=2'd0) ||
					(LH || LHU) && (EX_AGUVaddr[0]!=1'd0);
	wire	AdES =  SW && (EX_AGUVaddr[1:0]!=2'd0) ||
					SH && (EX_AGUVaddr[0]!=1'd0);
					
	wire	[4:0]	EX_AdEcode = (AdEL)?5'd4:
									(AdES)?5'd5:5'd0;
	
	//EXFinished6
	wire			EXFinished6		= AdEL || AdES;
	wire	[31:0]	EXF6_BadVAddr	= EX_AGUVaddr;
	wire	[7:0]	EXF6_EXCCODE	= {EXFinished6, EX_EXCCODE3[6:0], EX_AdEcode};
	wire	[5:0]	EXF6_ReOrderNum	= EX_ReOrderNum3;
	
	//Inst4
	wire			EX_valid4;
	wire	[31:0]	EX_PC4;
	wire	[31:0]	EX_Inst4;
	wire	[4:0]	EX_alcode4;
	wire	[4:0]	EX_mdcode4;
	wire	[4:0]	EX_memcode4;
	wire	[31:0]	EX_operand7;
	wire	[31:0]	EX_operand8;
	wire	[63:0]	EX_HILOdata4;
	wire	[4:0]	EX_regdes4;
	wire	[5:0]	EX_pregdes4;
	wire	[5:0]	EX_ppregdes4;
	wire	[5:0]	EX_ReOrderNum4;
	wire	[7:0]	EX_EXCCODE4;
	
RREX CORE_RREX4(

	.i_RREX_clk(		EX_clk),
	.i_RREX_enable(		EX_enable),
	.i_RREX_reset(		EX_reset),
	
	.i_RREX_valid(		RR_valid4),
	.i_RREX_PC(			RR_PC4),
	.i_RREX_Inst(		RR_Inst4),
	.i_RREX_alcode(		RR_alcode4),
	.i_RREX_mdcode(		RR_mdcode4),
	.i_RREX_memcode(	RR_memcode4),
	.i_RREX_operand1(	RR_rdata7),
	.i_RREX_operand2(	RR_rdata8),
	.i_RREX_HILOdata(	RR_HILOdata),
	.i_RREX_regdes(		RR_regdes4),
	.i_RREX_pregdes(	RR_pregdes4),
	.i_RREX_ppregdes(	RR_ppregdes4),
	.i_RREX_ReOrderNum(	RR_ReOrderNum4),
	.i_RREX_EXCCODE(	RR_EXCCODE4),
	
	.o_RREX_valid(		EX_valid4),
	.o_RREX_PC(			EX_PC4),
	.o_RREX_Inst(		EX_Inst4),
	.o_RREX_alcode(		EX_alcode4),
	.o_RREX_mdcode(		EX_mdcode4),
	.o_RREX_memcode(	EX_memcode4),
	.o_RREX_operand1(	EX_operand7),
	.o_RREX_operand2(	EX_operand8),
	.o_RREX_HILOdata(	EX_HILOdata4),
	.o_RREX_regdes(		EX_regdes4),
	.o_RREX_pregdes(	EX_pregdes4),
	.o_RREX_ppregdes(	EX_ppregdes4),
	.o_RREX_ReOrderNum(	EX_ReOrderNum4),
	.o_RREX_EXCCODE(	EX_EXCCODE4)

);

MultiplicationDivisionUnit CORE_MDU(
	
	.i_MDU_clk(			Ex),
	.i_MDU_enable(		),
	.i_MDU_reset(		),
	
	.i_MDU_mdcode(		),
	.i_MDU_operand1(	),
	.i_MDU_operand2(	),
	.i_MDU_HILOdata(	),
	
	.o_MDU_stall(		),
	.o_MDU_result(		)
	
);
	/*
		<--------MemoryMap-------->
	*/
EXMM CORE_EXMM(

	.i_EXMM_clk(),
	.i_EXMM_enable(),
	.i_EXMM_reset(),
	
	.i_EXMM_valid(),
	.i_EXMM_PC(),
	.i_EXMM_Inst(),
	.i_EXMM_memcode(),
	.i_EXMM_VAddr(),
	.i_EXMM_Size(),
	.i_EXMM_StoreData(),
	.i_EXMM_regdes(),
	.i_EXMM_pregdes(),
	.i_EXMM_ppregdes(),
	.i_EXMM_ReOrderNum(),
	.i_EXMM_EXCCODE(),		//for TLB
	
	.o_EXMM_valid(),
	.o_EXMM_PC(),
	.o_EXMM_Inst(),
	.o_EXMM_memcode(),
	.o_EXMM_VAddr(),
	.o_EXMM_Size(),
	.o_EXMM_StoreData(),
	.o_EXMM_regdes(),
	.o_EXMM_pregdes(),
	.o_EXMM_ppregdes(),
	.o_EXMM_ReOrderNum(),
	.o_EXMM_EXCCODE()

);

MemoryManagementUnit CORE_DMMU(

	.i_MMU_Vaddr(),
	.o_MMU_Paddr()

);

	/*
		<--------LoadStore-------->
	*/
MMLS CORE_MMLS(

	.i_MMLS_clk(),
	.i_MMLS_enable(),
	.i_MMLS_reset(),
	
	.i_MMLS_valid(),
	.i_MMLS_PC(),
	.i_MMLS_Inst(),
	.i_MMLS_memcode(),
	.i_MMLS_PAddr(),
	.i_MMLS_Size(),
	.i_MMLS_regdes(),
	.i_MMLS_pregdes(),
	.i_MMLS_ppregdes(),
	.i_MMLS_ReOrderNum(),
	
	.o_MMLS_valid(),
	.o_MMLS_PC(),
	.o_MMLS_Inst(),
	.o_MMLS_memcode(),
	.o_MMLS_PAddr(),
	.o_MMLS_Size(),
	.o_MMLS_regdes(),
	.o_MMLS_pregdes(),
	.o_MMLS_ppregdes(),
	.o_MMLS_ReOrderNum()

);

StoreBuffer CORE_SB(

	.i_SB_clk(),
	.i_SB_enable(),
	.i_SB_reset(),
	//Stall
	.o_SB_stall(),
	//EnQueue
	.i_SB_valid1(),
	.i_SB_StoreSize1(),
	.i_SB_StorePAddr1(),
	.i_SB_StoreData1(),
	.i_SB_valid2(),
	.i_SB_StoreSize2(),
	.i_SB_StorePAddr2(),
	.i_SB_StoreData2(),
	//DeQueue
	.o_SB_valid(),
	.o_SB_StoreSize(),
	.o_SB_StorePAddr(),
	.o_SB_StoreData(),
	.i_SB_DataOK()

);

	/*
		<--------Commit-------->
	*/
ReOrderBuffer CORE_ROB(
	
	.i_ROB_clk(),
	.i_ROB_enable(),
	.i_ROB_reset(),
	//Stall
	.o_ROB_Stall(),
	//Load and Hit
	.i_ROB_LoadPAddr(),
	.i_ROB_LoadReOrderNum(	),
	.o_ROB_Hit(),
	//EnQueue
	.i_ROB_VIP1(),
	.o_ROB_ReOrderNum1(		ROB_ReOrderNum1),
	.i_ROB_VIP2(),
	.o_ROB_ReOrderNum2(		ROB_ReOrderNum2),
	//Finished
	.i_ROB_RRFinished1(),
	.i_ROB_RRF1_PRDes(),
	.i_ROB_RRF1_RFwbdata(),
	.i_ROB_RRF1_BRANCH(),
	.i_ROB_RRF1_EXCCODE(),
	.i_ROB_RRF1_ReOrderNum(	),
	
	.i_ROB_RRFinished2(),
	.i_ROB_RRF2_PRDes(),
	.i_ROB_RRF2_RFwbdata(),
	.i_ROB_RRF2_BRANCH(),
	.i_ROB_RRF2_EXCCODE(),
	.i_ROB_RRF2_ReOrderNum(),
	
	.i_ROB_RRFinished3(),
	.i_ROB_RRF3_PRDes(),
	.i_ROB_RRF3_RFwbdata(),
	.i_ROB_RRF3_CP0w(),
	.i_ROB_RRF3_HILOw(),
	.i_ROB_RRF3_ReOrderNum(),
	
	.i_ROB_EXFinished4(),
	.i_ROB_EXF4_PRDes(),
	.i_ROB_EXF4_RFwbdata(),
	.i_ROB_EXF4_EXCCODE(),
	.i_ROB_EXF4_ReOrderNum(),
	
	.i_ROB_EXFinished5(),
	.i_ROB_EXF5_PRDes(),
	.i_ROB_EXF5_RFwbdata(),
	.i_ROB_EXF5_EXCCODE(),
	.i_ROB_EXF5_ReOrderNum(),
	
	.i_ROB_EXFinished6(),
	.i_ROB_EXF6_BadVAddr(),
	.i_ROB_EXF6_EXCCODE(),
	._ROB_EXF6_ReOrderNum(),
	
	.i_ROB_EXFinished7(),
	.i_ROB_EXF7_ReOrderNum(),
	
	.i_ROB_MMFinished8(),
	.i_ROB_MMF8_STORE(),
	.i_ROB_MMF8_EXCCODE(),
	.i_ROB_MMF8_ReOrderNum(),
	
	.i_ROB_LSFinished9(),
	.i_ROB_LSF9_PRDes(),
	.i_ROB_LSF9_RFwbdata(),
	.i_ROB_LSF9_ReOrderNum(),
	
	//DeQueue --> CMOMIT
	.o_ROB_CMT_A_VIP(),			//{[64]valid, [63:32]Inst, [31:0]PC}
	.o_ROB_CMT_A_PRDes(),		//{[16:12]regdes, [11:6]pregdes, [5:0]ppregdes}
	.o_ROB_CMT_A_RFwbdata(),	//debug_wb_rf_wdata
	.o_ROB_CMT_A_BRANCH(),		//{[34]BranchEnable, [33:32]BranchTaken, [31:0]BranchTarget}					
	.o_ROB_CMT_A_STORE(),		//{[67]StoreEnable, [66:64]StoreSize, [63:32]StorePAddr, [31:0]StoreData}						
	.o_ROB_CMT_A_CP0w(),		//{[40]CP0wen, [39:35]CP0addr, [34:32]CP0sel, [31:0]CP0data}
	.o_ROB_CMT_A_HILOw(),		//{[65:64]HILOwen, [63:0]HILOwdata}
	.o_ROB_CMT_A_BadVAddr(),
	.o_ROB_CMT_A_EXCCODE(),
	.o_ROB_CMT_B_VIP(),			//{[64]valid, [63:32]Inst, [31:0]PC}
	.o_ROB_CMT_B_PRDes(),		//{[16:12]regdes, [11:6]pregdes, [5:0]ppregdes}
	.o_ROB_CMT_B_RFwbdata(),	//debug_wb_rf_wdata
	.o_ROB_CMT_B_BRANCH(),		//{[34]BranchEnable, [33:32]BranchTaken, [31:0]BranchTarget}					
	.o_ROB_CMT_B_STORE(),		//{[67]StoreEnable, [66:64]StoreSize, [63:32]StorePAddr, [31:0]StoreData}						
	.o_ROB_CMT_B_CP0w(),		//{[40]CP0wen, [39:35]CP0addr, [34:32]CP0sel, [31:0]CP0data}
	.o_ROB_CMT_B_HILOw(),		//{[65:64]HILOwen, [63:0]HILOwdata}
	.o_ROB_CMT_B_BadVAddr(),
	.o_ROB_CMT_B_EXCCODE()
	
);

endmodule