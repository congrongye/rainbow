module RNSC(
	
	input			i_RNSC_clk,
	input			i_RNSC_enable,
	input			i_RNSC_reset,
	
	input			i_RNSC_valid,
	input	[31:0]	i_RNSC_PC,
	input	[31:0]	i_RNSC_Inst,
	input	[4:0]	i_RNSC_alcode,
	input	[4:0]	i_RNSC_bjcode,
	input	[4:0]	i_RNSC_iecode,
	input	[4:0]	i_RNSC_mvcode,
	input	[4:0]	i_RNSC_mdcode,
	input	[4:0]	i_RNSC_memcode,
	input	[5:0]	i_RNSC_pregsrc1,
	input	[5:0]	i_RNSC_pregsrc2,
	input	[4:0]	i_RNSC_regdes,
	input	[5:0]	i_RNSC_pregdes,
	input	[5:0]	i_RNSC_ppregdes,
	input	[1:0]	i_RNSC_BranchTaken,
	input	[31:0]	i_RNSC_BranchTarget,
	input	[5:0]	i_RNSC_ReOrderNum,
	input	[7:0]	i_RNSC_EXCCODE,
	
	output			o_RNSC_valid,
	output	[31:0]	o_RNSC_PC,
	output	[31:0]	o_RNSC_Inst,
	output	[4:0]	o_RNSC_alcode,
	output	[4:0]	o_RNSC_bjcode,
	output	[4:0]	o_RNSC_iecode,
	output	[4:0]	o_RNSC_mvcode,
	output	[4:0]	o_RNSC_mdcode,
	output	[4:0]	o_RNSC_memcode,
	output	[5:0]	o_RNSC_pregsrc1,
	output	[5:0]	o_RNSC_pregsrc2,
	output	[4:0]	o_RNSC_regdes,
	output	[5:0]	o_RNSC_pregdes,
	output	[5:0]	o_RNSC_ppregdes,
	output	[1:0]	o_RNSC_BranchTaken,
	output	[31:0]	o_RNSC_BranchTarget,
	output	[5:0]	o_RNSC_ReOrderNum,
	output	[7:0]	o_RNSC_EXCCODE

);

	reg			valid;
	reg	[31:0]	PC;
	reg	[31:0]	Inst;
	reg	[4:0]	alcode;
	reg	[4:0]	bjcode;
	reg	[4:0]	iecode;
	reg	[4:0]	mvcode;
	reg	[4:0]	mdcode;
	reg	[4:0]	memcode;
	reg	[5:0]	pregsrc1;
	reg	[5:0]	pregsrc2;
	reg	[4:0]	regdes;
	reg	[5:0]	pregdes;
	reg	[5:0]	ppregdes;
	reg	[1:0]	BranchTaken;
	reg	[31:0]	BranchTarget;
	reg	[5:0]	ReOrderNum;
	reg	[7:0]	EXCCODE;

always @(posedge i_RNSC_clk)
	begin
		if(i_RNSC_reset)
			begin
				valid			<= 1'd0;
				PC				<= 32'd0;
				Inst			<= 32'd0;
				alcode			<= 5'd0;
				bjcode			<= 5'd0;
				iecode			<= 5'd0;
				mvcode			<= 5'd0;
				mdcode			<= 5'd0;
				memcode			<= 5'd0;
				pregsrc1		<= 6'd0;
				pregsrc2		<= 6'd0;
				regdes			<= 5'd0;
				pregdes			<= 6'd0;
				ppregdes		<= 6'd0;
				BranchTaken		<= 2'd0;
				BranchTarget	<= 32'd0;
				EXCCODE			<= 8'd0;
				ReOrderNum		<= 6'd0;
			end
		else if(i_RNSC_enable)
			begin
				valid			<= i_RNSC_valid;
				PC				<= i_RNSC_PC;
				Inst			<= i_RNSC_Inst;
				alcode			<= i_RNSC_alcode;
				bjcode			<= i_RNSC_bjcode;
				iecode			<= i_RNSC_iecode;
				mvcode			<= i_RNSC_mvcode;
				mdcode			<= i_RNSC_mdcode;
				memcode			<= i_RNSC_memcode;
				pregsrc1		<= i_RNSC_pregsrc1;
				pregsrc2		<= i_RNSC_pregsrc2;
				regdes			<= i_RNSC_regdes;
				pregdes			<= i_RNSC_pregdes;
				ppregdes		<= i_RNSC_ppregdes;
				BranchTaken		<= i_RNSC_BranchTaken;
				BranchTarget	<= i_RNSC_BranchTarget;
				ReOrderNum		<= i_RNSC_ReOrderNum;
				EXCCODE			<= i_RNSC_EXCCODE;
			end
	end

endmodule