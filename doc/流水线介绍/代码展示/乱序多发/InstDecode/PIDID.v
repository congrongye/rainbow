module PIDID(
	
	input			i_PIDID_clk,
	input			i_PIDID_enable,
	input			i_PIDID_reset,
	
	input			i_PIDID_valid,
	input	[31:0]	i_PIDID_PC,
	input	[31:0]	i_PIDID_Inst,
	input			i_PIDID_BranchJump,
	input			i_PIDID_BranchDelaySlot,
	input	[1:0]	i_PIDID_BranchTaken,
	input	[31:0]	i_PIDID_BranchTarget,
	
	output			o_PIDID_valid,
	output	[31:0]	o_PIDID_PC,
	output	[31:0]	o_PIDID_Inst,
	output	[4:0]	o_PIDID_BranchJump,
	output			o_PIDID_BranchDelaySlot,
	output	[1:0]	o_PIDID_BranchTaken,
	output	[31:0]	o_PIDID_BranchTarget
	
);

	reg			valid;
	reg	[31:0]	PC;
	reg	[31:0]	Inst;
	reg			BranchJump,
	reg			BranchDelaySlot;
	reg	[1:0]	BranchTaken;
	reg	[31:0]	BranchTarget;

	assign	o_PIDID_valid			= valid;
	assign	o_PIDID_PC				= PC;
	assign	o_PIDID_Inst			= Inst;
	output	o_PIDID_BranchJump		= BranchJump;
	assign	o_PIDID_BranchDelaySlot	= BranchDelaySlot;
	assign	o_PIDID_BranchTaken		= BranchTaken;
	assign	o_PIDID_BranchTarget	= BranchTarget;

always @(posedge i_PIDID_clk)
	begin
		if(i_PIDID_reset)
			begin
				valid			<= 1'd0;
				PC				<= 32'd0;
				Inst			<= 32'd0;
				BranchJump		<= 1'd0;
				BranchDelaySlot	<= 1'd0;
				BranchTaken		<= 2'd0;
				BranchTarget	<= 32'd0;
			end
		else if(i_PIDID_enable)
			begin
				valid			<= i_PIDID_valid;
				PC				<= i_PIDID_PC;
				Inst			<= i_PIDID_Inst;
				BranchJump		<= i_PIDID_BranchJump;
				BranchDelaySlot	<= i_PIDID_BranchDelaySlot;
				BranchTaken		<= i_PIDID_BranchTaken;
				BranchTarget	<= i_PIDID_BranchTarget;
			end
	end

endmodule