module phyregfile(

	input			i_prf_clk,
	input			i_prf_enable,
	input			i_prf_reset,
	//read
	input	[5:0]	i_prf_raddr1,
	input	[5:0]	i_prf_raddr2,
	input	[5:0]	i_prf_raddr3,
	input	[5:0]	i_prf_raddr4,
	input	[5:0]	i_prf_raddr5,
	output	[31:0]	o_prf_rdata1,
	output	[31:0]	o_prf_rdata2,
	output	[31:0]	o_prf_rdata3,
	output	[31:0]	o_prf_rdata4,
	output	[31:0]	o_prf_rdata5,
	//write
	input	[5:0]	i_prf_waddr1,
	input	[5:0]	i_prf_waddr2,
	input	[5:0]	i_prf_waddr3,
	input	[5:0]	i_prf_waddr4,
	input	[5:0]	i_prf_waddr5,
	input	[5:0]	i_prf_waddr6,
	input	[5:0]	i_prf_waddr7,
	input	[31:0]	i_prf_wdata1,
	input	[31:0]	i_prf_wdata2,
	input	[31:0]	i_prf_wdata3,
	input	[31:0]	i_prf_wdata4,
	input	[31:0]	i_prf_wdata5,
	input	[31:0]	i_prf_wdata6,
	input	[31:0]	i_prf_wdata7
	
);

	reg	[31:0]	PhyReg[63:0];
	
	assign	o_prf_rdata1 = PhyReg[i_prf_raddr1];
	assign	o_prf_rdata2 = PhyReg[i_prf_raddr2];
	assign	o_prf_rdata3 = PhyReg[i_prf_raddr3];
	assign	o_prf_rdata4 = PhyReg[i_prf_raddr4];
	assign	o_prf_rdata5 = PhyReg[i_prf_raddr5];

always @(posedge i_prf_clk)
	begin
		if(i_prf_reset)
			begin:	reset
				reg	[7:0]	tempI;
				for(tempI=8'd0; tempI<=8'd63; tempI=tempI+8'd1)
					PhyReg[tempI] <= 32'd0;
			end
		else if(i_prf_enable)
			begin
				if(i_prf_waddr1) PhyReg[i_prf_waddr1] <= i_prf_wdata1;
				if(i_prf_waddr2) PhyReg[i_prf_waddr2] <= i_prf_wdata2;
				if(i_prf_waddr3) PhyReg[i_prf_waddr3] <= i_prf_wdata3;
				if(i_prf_waddr4) PhyReg[i_prf_waddr4] <= i_prf_wdata4;
				if(i_prf_waddr5) PhyReg[i_prf_waddr5] <= i_prf_wdata5;
				if(i_prf_waddr6) PhyReg[i_prf_waddr6] <= i_prf_wdata6;
				if(i_prf_waddr7) PhyReg[i_prf_waddr7] <= i_prf_wdata7;
			end
	end

endmodule