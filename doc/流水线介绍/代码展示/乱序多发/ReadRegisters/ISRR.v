module ISRR(
	
	input			i_ISRR_clk,
	input			i_ISRR_enable,
	input			i_ISRR_reset,
	
	input			i_ISRR_valid,
	input	[31:0]	i_ISRR_PC,
	input	[31:0]	i_ISRR_Inst,
	input	[4:0]	i_ISRR_alcode,
	input	[4:0]	i_ISRR_bjcode,
	input	[4:0]	i_ISRR_iecode,
	input	[4:0]	i_ISRR_mvcode,
	input	[4:0]	i_ISRR_mdcode,
	input	[4:0]	i_ISRR_memcode,
	input	[5:0]	i_ISRR_pregsrc1,
	input	[5:0]	i_ISRR_pregsrc2,
	input	[4:0]	i_ISRR_regdes,
	input	[5:0]	i_ISRR_pregdes,
	input	[5:0]	i_ISRR_ppregdes,
	input	[1:0]	i_ISRR_BranchTaken,
	input	[31:0]	i_ISRR_BranchTarget,
	input	[5:0]	i_ISRR_ReOrderNum,
	input	[7:0]	i_ISRR_EXCCODE,
	
	output			o_ISRR_valid,
	output	[31:0]	o_ISRR_PC,
	output	[31:0]	o_ISRR_Inst,
	output	[4:0]	o_ISRR_alcode,
	output	[4:0]	o_ISRR_bjcode,
	output	[4:0]	o_ISRR_iecode,
	output	[4:0]	o_ISRR_mvcode,
	output	[4:0]	o_ISRR_mdcode,
	output	[4:0]	o_ISRR_memcode,
	output	[5:0]	o_ISRR_pregsrc1,
	output	[5:0]	o_ISRR_pregsrc2,
	output	[4:0]	o_ISRR_regdes,
	output	[5:0]	o_ISRR_pregdes,
	output	[5:0]	o_ISRR_ppregdes,
	output	[1:0]	o_ISRR_BranchTaken,
	output	[31:0]	o_ISRR_BranchTarget,
	output	[5:0]	o_ISRR_ReOrderNum,
	output	[7:0]	o_ISRR_EXCCODE

);

	reg			valid;
	reg	[31:0]	PC;
	reg	[31:0]	Inst;
	reg	[4:0]	alcode;
	reg	[4:0]	bjcode;
	reg	[4:0]	iecode;
	reg	[4:0]	mvcode;
	reg	[4:0]	mdcode;
	reg	[4:0]	memcode;
	reg	[5:0]	pregsrc1;
	reg	[5:0]	pregsrc2;
	reg	[4:0]	regdes;
	reg	[5:0]	pregdes;
	reg	[5:0]	ppregdes;
	reg	[1:0]	BranchTaken;
	reg	[31:0]	BranchTarget;
	reg	[5:0]	ReOrderNum;
	reg	[7:0]	EXCCODE;

always @(posedge i_ISRR_clk)
	begin
		if(i_ISRR_reset)
			begin
				valid			<= 1'd0;
				PC				<= 32'd0;
				Inst			<= 32'd0;
				alcode			<= 5'd0;
				bjcode			<= 5'd0;
				iecode			<= 5'd0;
				mvcode			<= 5'd0;
				mdcode			<= 5'd0;
				memcode			<= 5'd0;
				pregsrc1		<= 6'd0;
				pregsrc2		<= 6'd0;
				regdes			<= 5'd0;
				pregdes			<= 6'd0;
				ppregdes		<= 6'd0;
				BranchTaken		<= 2'd0;
				BranchTarget	<= 32'd0;
				EXCCODE			<= 7'd0;
				ReOrderNum		<= 6'd0;
			end
		else if(i_ISRR_enable)
			begin
				valid			<= i_ISRR_valid;
				PC				<= i_ISRR_PC;
				Inst			<= i_ISRR_Inst;
				alcode			<= i_ISRR_alcode;
				bjcode			<= i_ISRR_bjcode;
				iecode			<= i_ISRR_iecode;
				mvcode			<= i_ISRR_mvcode;
				mdcode			<= i_ISRR_mdcode;
				memcode			<= i_ISRR_memcode;
				pregsrc1		<= i_ISRR_pregsrc1;
				pregsrc2		<= i_ISRR_pregsrc2;
				regdes			<= i_ISRR_regdes;
				pregdes			<= i_ISRR_pregdes;
				ppregdes		<= i_ISRR_ppregdes;
				BranchTaken		<= i_ISRR_BranchTaken;
				BranchTarget	<= i_ISRR_BranchTarget;
				ReOrderNum		<= i_ISRR_ReOrderNum;
				EXCCODE			<= i_ISRR_EXCCODE;
			end
	end

endmodule