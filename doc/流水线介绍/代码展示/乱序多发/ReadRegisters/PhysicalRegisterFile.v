module PhysicalRegisterFile(

	input			i_PRF_clk,
	input			i_PRF_enable,
	input			i_PRF_reset,
	//read
	input	[5:0]	i_PRF_raddr1,
	input	[5:0]	i_PRF_raddr2,
	input	[5:0]	i_PRF_raddr3,
	input	[5:0]	i_PRF_raddr4,
	input	[5:0]	i_PRF_raddr5,
	input	[5:0]	i_PRF_raddr6,
	input	[5:0]	i_PRF_raddr7,
	input	[5:0]	i_PRF_raddr8,
	input	[5:0]	i_PRF_raddr9,
	input	[5:0]	i_PRF_raddr10,
	output	[31:0]	o_PRF_rdata1,
	output	[31:0]	o_PRF_rdata2,
	output	[31:0]	o_PRF_rdata3,
	output	[31:0]	o_PRF_rdata4,
	output	[31:0]	o_PRF_rdata5,
	output	[31:0]	o_PRF_rdata6,
	output	[31:0]	o_PRF_rdata7,
	output	[31:0]	o_PRF_rdata8,
	output	[31:0]	o_PRF_rdata9,
	output	[31:0]	o_PRF_rdata10,
	//write
	input	[5:0]	i_PRF_waddr1,
	input	[5:0]	i_PRF_waddr2,
	input	[5:0]	i_PRF_waddr3,
	input	[5:0]	i_PRF_waddr4,
	input	[5:0]	i_PRF_waddr5,
	input	[5:0]	i_PRF_waddr6,
	input	[5:0]	i_PRF_waddr7,
	input	[31:0]	i_PRF_wdata1,
	input	[31:0]	i_PRF_wdata2,
	input	[31:0]	i_PRF_wdata3,
	input	[31:0]	i_PRF_wdata4,
	input	[31:0]	i_PRF_wdata5,
	input	[31:0]	i_PRF_wdata6,
	input	[31:0]	i_PRF_wdata7
	
);

phyregfile PRF_prf_0105(
	.i_prf_clk(		i_PRF_clk),
	.i_prf_enable(	i_PRF_enable),
	.i_prf_reset(	i_PRF_reset),
	
	.i_prf_raddr1(	i_PRF_raddr1),
	.i_prf_raddr2(	i_PRF_raddr2),
	.i_prf_raddr3(	i_PRF_raddr3),
	.i_prf_raddr4(	i_PRF_raddr4),
	.i_prf_raddr5(	i_PRF_raddr5),
	.o_prf_rdata1(	o_PRF_rdata1),
	.o_prf_rdata2(	o_PRF_rdata2),
	.o_prf_rdata3(	o_PRF_rdata3),
	.o_prf_rdata4(	o_PRF_rdata4),
	.o_prf_rdata5(	o_PRF_rdata5),
	
	.i_prf_waddr1(	i_PRF_waddr1),
	.i_prf_waddr2(	i_PRF_waddr2),
	.i_prf_waddr3(	i_PRF_waddr3),
	.i_prf_waddr4(	i_PRF_waddr4),
	.i_prf_waddr5(	i_PRF_waddr5),
	.i_prf_waddr6(	i_PRF_waddr6),
	.i_prf_waddr7(	i_PRF_waddr7),
	.i_prf_wdata1(	i_PRF_wdata1),
	.i_prf_wdata2(	i_PRF_wdata2),
	.i_prf_wdata3(	i_PRF_wdata3),
	.i_prf_wdata4(	i_PRF_wdata4),
	.i_prf_wdata5(	i_PRF_wdata5),
	.i_prf_wdata6(	i_PRF_wdata6),
	.i_prf_wdata7(	i_PRF_wdata7)

);

phyregfile PRF_prf_0610(
	.i_prf_clk(		i_PRF_clk),
	.i_prf_enable(	i_PRF_enable),
	.i_prf_reset(	i_PRF_reset),
	
	.i_prf_raddr1(	i_PRF_raddr6),
	.i_prf_raddr2(	i_PRF_raddr7),
	.i_prf_raddr3(	i_PRF_raddr8),
	.i_prf_raddr4(	i_PRF_raddr9),
	.i_prf_raddr5(	i_PRF_raddr10),
	.o_prf_rdata1(	o_PRF_rdata6),
	.o_prf_rdata2(	o_PRF_rdata7),
	.o_prf_rdata3(	o_PRF_rdata8),
	.o_prf_rdata4(	o_PRF_rdata9),
	.o_prf_rdata5(	o_PRF_rdata10),
	
	.i_prf_waddr1(	i_PRF_waddr1),
	.i_prf_waddr2(	i_PRF_waddr2),
	.i_prf_waddr3(	i_PRF_waddr3),
	.i_prf_waddr4(	i_PRF_waddr4),
	.i_prf_waddr5(	i_PRF_waddr5),
	.i_prf_waddr6(	i_PRF_waddr6),
	.i_prf_waddr7(	i_PRF_waddr7),
	.i_prf_wdata1(	i_PRF_wdata1),
	.i_prf_wdata2(	i_PRF_wdata2),
	.i_prf_wdata3(	i_PRF_wdata3),
	.i_prf_wdata4(	i_PRF_wdata4),
	.i_prf_wdata5(	i_PRF_wdata5),
	.i_prf_wdata6(	i_PRF_wdata6),
	.i_prf_wdata7(	i_PRF_wdata7)

);

endmodule