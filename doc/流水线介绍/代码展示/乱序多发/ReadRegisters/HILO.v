module HILO(
	
	input			i_HILO_clk,
	input			i_HILO_enable,
	input			i_HILO_reset,
	
	input	[1:0]	i_HILO_HILOwen,
	input	[63:0]	i_HILO_HILOwdata,
	
	output	[63:0]	o_HILO_HILOdata,
	//COMMIT
	input	[63:0]	i_HILO_CMTrollback,
	input	[1:0]	i_HILO_CMTHILOwen,
	input	[63:0]	i_HILO_CMTHILOwdata
	
);
	
	reg	[31:0] HI;
	reg	[31:0] LO;
	reg	[31:0] HI_rec;
	reg	[31:0] LO_rec;
	
	assign	o_HILO_HILOdata[63:32] 	= HI;
	assign	o_HILO_HILOdata[31:0] 	= LO;
	
always @(posedge i_HILO_clk)
	if(i_HILO_reset)
		begin
			HI <= 32'd0;
			LO <= 32'd0;
			HI_rec <= 32'd0;
			LO_rec <= 32'd0;
		end
	else if(i_HILO_enable)
		begin
			if(i_HILO_CMTrollback)
				begin
					HI <= HI_rec;
					LO <= LO_rec;
				end
			else
				begin
					//write
					if(i_HILO_HILOwen[1]) HI <= i_HILO_HILOwdata[63:32];
					if(i_HILO_HILOwen[0]) LO <= i_HILO_HILOwdata[31:0];
					//COMMIT
					if(i_HILO_CMTHILOwen[1]) HI_rec <= i_HILO_CMTHILOwdata[63:32];
					if(i_HILO_CMTHILOwen[0]) LO_rec <= i_HILO_CMTHILOwdata[31:0];
				end
		end

endmodule