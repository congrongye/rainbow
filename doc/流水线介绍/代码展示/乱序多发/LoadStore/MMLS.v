module MMLS(

	input			i_MMLS_clk,
	input			i_MMLS_enable,
	input			i_MMLS_reset,
	
	input			i_MMLS_valid,
	input	[31:0]	i_MMLS_PC,
	input	[31:0]	i_MMLS_Inst,
	input	[4:0]	i_MMLS_memcode,
	input	[31:0]	i_MMLS_PAddr,
	input	[2:0]	i_MMLS_Size,
	input	[4:0]	i_MMLS_regdes,
	input	[5:0]	i_MMLS_pregdes,
	input	[5:0]	i_MMLS_ppregdes,
	input	[5:0]	i_MMLS_ReOrderNum,
	
	output			o_MMLS_valid,
	output	[31:0]	o_MMLS_PC,
	output	[31:0]	o_MMLS_Inst,
	output	[4:0]	o_MMLS_memcode,
	output	[31:0]	o_MMLS_PAddr,
	output	[2:0]	o_MMLS_Size,
	output	[4:0]	o_MMLS_regdes,
	output	[5:0]	o_MMLS_pregdes,
	output	[5:0]	o_MMLS_ppregdes,
	output	[5:0]	o_MMLS_ReOrderNum

);

	reg			valid;
	reg	[31:0]	PC;
	reg	[31:0]	Inst;
	reg	[4:0]	memcode;
	reg	[31:0]	PAddr;
	reg	[2:0]	Size;
	reg	[4:0]	regdes;
	reg	[5:0]	pregdes;
	reg	[5:0]	ppregdes;
	reg	[5:0]	ReOrderNum;

always @(i_MMLS_clk)
	begin
		if(i_MMLS_reset)
			begin
				valid		<= 1'd0;
				PC			<= 32'd0;
				Inst		<= 32'd0;
				memcode		<= 5'd0;
				PAddr		<= 32'd0;
				Size		<= 3'd0;
				regdes		<= 5'd0;
				pregdes		<= 6'd0;
				ppregdes	<= 6'd0;
				ReOrderNum	<= 6'd0;
			end
		else if(i_MMLS_enable)
			begin
				valid		<= i_MMLS_valid;
				PC			<= i_MMLS_PC;
				Inst		<= i_MMLS_Inst;
				memcode		<= i_MMLS_memcode;
				PAddr		<= i_MMLS_PAddr;
				Size		<= i_MMLS_Size;
				regdes		<= i_MMLS_regdes;
				pregdes		<= i_MMLS_pregdes;
				ppregdes	<= i_MMLS_ppregdes;
				ReOrderNum	<= i_MMLS_ReOrderNum;
			end
	end

endmodule