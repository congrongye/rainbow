module MemoryManagementUnit(

	input	[31:0]	i_MMU_Vaddr,
	output	[31:0]	o_MMU_Paddr

);

	/*
									1000----1010    --> 0000----0010
									
									1010----1100    --> 0000----0010
	*/
	
	assign	o_MMU_Paddr = (i_MMU_Vaddr >= 32'h8000_0000 && i_MMU_Vaddr < 32'ha000_0000)
							?
							(i_MMU_Vaddr - 32'h8000_0000)
							:
							((i_MMU_Vaddr >= 32'ha000_0000 && i_MMU_Vaddr < 32'hc000_0000) 
								? 
								(i_MMU_Vaddr - 32'ha000_0000)
								:
								i_MMU_Vaddr
							);

endmodule