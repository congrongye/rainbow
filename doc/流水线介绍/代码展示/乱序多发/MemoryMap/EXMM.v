module EXMM(

	input			i_EXMM_clk,
	input			i_EXMM_enable,
	input			i_EXMM_reset,
	
	input			i_EXMM_valid,
	input	[31:0]	i_EXMM_PC,
	input	[31:0]	i_EXMM_Inst,
	input	[4:0]	i_EXMM_memcode,
	input	[31:0]	i_EXMM_VAddr,
	input	[2:0]	i_EXMM_Size,
	input	[31:0]	i_EXMM_StoreData,
	input	[4:0]	i_EXMM_regdes,
	input	[5:0]	i_EXMM_pregdes,
	input	[5:0]	i_EXMM_ppregdes,
	input	[5:0]	i_EXMM_ReOrderNum,
	input	[7:0]	i_EXMM_EXCCODE,		//for TLB
	
	output			o_EXMM_valid,
	output	[31:0]	o_EXMM_PC,
	output	[31:0]	o_EXMM_Inst,
	output	[4:0]	o_EXMM_memcode,
	output	[31:0]	o_EXMM_VAddr,
	output	[2:0]	o_EXMM_Size,
	output	[31:0]	o_EXMM_StoreData,
	output	[4:0]	o_EXMM_regdes,
	output	[5:0]	o_EXMM_pregdes,
	output	[5:0]	o_EXMM_ppregdes,
	output	[5:0]	o_EXMM_ReOrderNum,
	output	[7:0]	o_EXMM_EXCCODE

);

	reg			valid;
	reg	[31:0]	PC;
	reg	[31:0]	Inst;
	reg	[4:0]	memcode;
	reg	[31:0]	VAddr;
	reg	[2:0]	Size;
	reg	[31:0]	StoreData;
	reg	[4:0]	regdes;
	reg	[5:0]	pregdes;
	reg	[5:0]	ppregdes;
	reg	[5:0]	ReOrderNum;
	reg	[7:0]	EXCCODE;

always @(i_EXMM_clk)
	begin
		if(i_EXMM_reset)
			begin
				valid		<= 1'd0;
				PC			<= 32'd0;
				Inst		<= 32'd0;
				memcode		<= 5'd0;
				VAddr		<= 32'd0;
				Size		<= 3'd0;
				StoreData	<= 32'd0;
				regdes		<= 5'd0;
				pregdes		<= 6'd0;
				ppregdes	<= 6'd0;
				ReOrderNum	<= 6'd0;
				EXCCODE		<= 8'd0;
			end
		else if(i_EXMM_enable)
			begin
				valid		<= i_EXMM_valid;
				PC			<= i_EXMM_PC;
				Inst		<= i_EXMM_Inst;
				memcode		<= i_EXMM_memcode;
				VAddr		<= i_EXMM_VAddr;
				Size		<= i_EXMM_Size;
				StoreData	<= i_EXMM_StoreData;
				regdes		<= i_EXMM_regdes;
				pregdes		<= i_EXMM_pregdes;
				ppregdes	<= i_EXMM_ppregdes;
				ReOrderNum	<= i_EXMM_ReOrderNum;
				EXCCODE		<= i_EXMM_EXCCODE;
			end
	end

endmodule