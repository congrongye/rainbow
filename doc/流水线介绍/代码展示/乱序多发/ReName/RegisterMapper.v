module RegisterMapper(
	
	input			i_RM_clk,
	input			i_RM_enable,
	input			i_RM_reset,
	//regsrc
	input	[4:0]	i_RM_regsrc1,
	input	[4:0]	i_RM_regsrc2,
	input	[4:0]	i_RM_regsrc3,
	input	[4:0]	i_RM_regsrc4,
	output	[5:0]	o_RM_pregsrc1,
	output	[5:0]	o_RM_pregsrc2,
	output	[5:0]	o_RM_pregsrc3,
	output	[5:0]	o_RM_pregsrc4,
	//regdes
	input	[4:0]	i_RM_regdes1,
	input	[4:0]	i_RM_regdes2,
	output	[5:0]	o_RM_pregdes1,
	output	[5:0]	o_RM_pregdes2,
	output	[5:0]	o_RM_ppregdes1,
	output	[5:0]	o_RM_ppregdes2,
	//RegExecouted
	input	[35:0]	i_RM_RegExecouted,
	//Full
	output			o_RM_stall,
	//PhyRegState
	output	[63:0]	o_RM_PhyRegState,
	//COMMIT
	input			CMTrollback,
	input	[5:0]	CMTregdes1,
	input	[5:0]	CMTregdes2,
	input	[5:0]	CMTpregdes1,
	input	[5:0]	CMTpregdes2,
	input	[5:0]	CMTppregdes1,
	input	[5:0]	CMTppregdes2
	
);

	wire	[5:0]	FreeReg1;
	wire	[5:0]	FreeReg2;
	assign	o_RM_pregdes1 = (i_RM_regdes1!=5'd0)? FreeReg1 : 6'd0;
	assign	o_RM_pregdes2 = (i_RM_regdes2!=5'd0)? FreeReg2 : 6'd0;;
	assign	o_RM_stall = (FreeReg1==6'd0 || FreeReg2==6'd0)
	
	wire	[5:0]	pregsrc1;
	wire	[5:0]	pregsrc2;
	wire	[5:0]	pregsrc3;
	wire	[5:0]	pregsrc4;
	assign	o_RM_pregsrc1 = pregsrc1;
	assign	o_RM_pregsrc2 = pregsrc2;
	assign	o_RM_pregsrc3 = (i_RM_regsrc3==i_RM_regdes1) ? FreeReg1 : pregsrc3;
	assign	o_RM_pregsrc4 = (i_RM_regsrc4==i_RM_regdes1) ? FreeReg1 : pregsrc4;

RenameTable RM_RT(
	.i_RT_clk(			i_RM_clk),
	.i_RT_enable(		i_RM_enable),
	.i_RT_reset(		i_RM_reset),
	//regsrc
	.i_RT_regsrc1(		i_RM_regsrc1),
	.i_RT_regsrc2(		i_RM_regsrc2),
	.i_RT_regsrc3(		i_RM_regsrc3),
	.i_RT_regsrc4(		i_RM_regsrc4),
	.o_RT_pregsrc1(		pregsrc1),
	.o_RT_pregsrc2(		pregsrc2),
	.o_RT_pregsrc3(		pregsrc3),
	.o_RT_pregsrc4(		pregsrc4),
	//regdes
	.i_RT_regdes1(		i_RM_regdes1),
	.i_RT_regdes2(		i_RM_regdes2),
	.o_RT_ppregdes1(	o_RM_ppregdes1),
	.o_RT_ppregdes2(	o_RM_ppregdes2),
	.i_RT_pregdes1(		FreeReg1),
	.i_RT_pregdes2(		FreeReg2),
	//COMMIT
	.i_RT_CMTregdes1(	CMTregdes1),
	.i_RT_CMTregdes2(	CMTregdes2),
	.i_RT_CMTpregdes1(	CMTpregdes1),
	.i_RT_CMTpregdes2(	CMTpregdes2),
	.i_RT_CMTrollback(	CMTrollback)
);

PhyRegStateTable RM_PRST(
	.i_PRST_clk(		i_RM_clk),
	.i_PRST_enable(		i_RM_enable),
	.i_PRST_reset(		i_RM_reset),
	//FreeReg
	.o_PRST_FreeReg1(	FreeReg1),
	.o_PRST_FreeReg2(	FreeReg2),
	//trans
	.i_PRST_Mappped(	{o_RM_pregdes1, o_RM_pregdes2}),
	.i_PRST_Executed(	i_RM_RegExecouted),
	.i_PRST_Assigned(	{CMTpregdes1, CMTpregdes2}),
	.i_PRST_Free(		{CMTppregdes1, CMTppregdes2}),
	.i_PRST_rollback(	CMTrollback),
	//PhyRegState
	.o_PRST_PhyRegState(o_RM_PhyRegState)
);



endmodule