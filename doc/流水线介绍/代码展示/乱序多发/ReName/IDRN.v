module IDRN(
	
	input			i_IDRN_clk,
	input			i_IDRN_enable,
	input			i_IDRN_reset,
	
	input			i_IDRN_valid,
	input	[31:0]	i_IDRN_PC,
	input	[31:0]	i_IDRN_Inst,
	input	[4:0]	i_IDRN_alcode,
	input	[4:0]	i_IDRN_bjcode,
	input	[4:0]	i_IDRN_iecode,
	input	[4:0]	i_IDRN_mvcode,
	input	[4:0]	i_IDRN_mdcode,
	input	[4:0]	i_IDRN_memcode,
	input	[4:0]	i_IDRN_regsrc1,
	input	[4:0]	i_IDRN_regsrc2,
	input	[4:0]	i_IDRN_regdes,
	input	[1:0]	i_IDRN_BranchTaken,
	input	[31:0]	i_IDRN_BranchTarget,
	input	[7:0]	i_IDRN_EXCCODE,
	
	output			o_IDRN_valid,
	output	[31:0]	o_IDRN_PC,
	output	[31:0]	o_IDRN_Inst,
	output	[4:0]	o_IDRN_alcode,
	output	[4:0]	o_IDRN_bjcode,
	output	[4:0]	o_IDRN_iecode,
	output	[4:0]	o_IDRN_mvcode,
	output	[4:0]	o_IDRN_mdcode,
	output	[4:0]	o_IDRN_memcode,
	output	[4:0]	o_IDRN_regsrc1,
	output	[4:0]	o_IDRN_regsrc2,
	output	[4:0]	o_IDRN_regdes,
	output	[1:0]	o_IDRN_BranchTaken,
	output	[31:0]	o_IDRN_BranchTarget,
	output	[7:0]	o_IDRN_EXCCODE
	
);

	reg			valid;
	reg	[31:0]	PC;
	reg	[31:0]	Inst;
	reg	[4:0]	alcode;
	reg	[4:0]	bjcode;
	reg	[4:0]	iecode;
	reg	[4:0]	mvcode;
	reg	[4:0]	mdcode;
	reg	[4:0]	memcode;
	reg	[4:0]	regsrc1;
	reg	[4:0]	regsrc2;
	reg	[4:0]	regdes;
	reg	[1:0]	BranchTaken;
	reg	[31:0]	BranchTarget;
	reg	[7:0]	EXCCODE;

always @(posedge i_IDRN_clk)
	begin
		if(i_IDRN_reset)
			begin
				valid			<= 1'd0;
				PC				<= 32'd0;
				Inst			<= 32'd0;
				alcode			<= 5'd0;
				bjcode			<= 5'd0;
				iecode			<= 5'd0;
				mvcode			<= 5'd0;
				mdcode			<= 5'd0;
				memcode			<= 5'd0;
				regsrc1			<= 5'd0;
				regsrc2			<= 5'd0;
				regdes			<= 5'd0;
				BranchTaken		<= 2'd0;
				BranchTarget	<= 32'd0;
				EXCCODE			<= 8'd0;
			end
		else if(i_IDRN_enable)
			begin
				valid			<= i_IDRN_valid;
				PC				<= i_IDRN_PC;
				Inst			<= i_IDRN_Inst;
				alcode			<= i_IDRN_alcode;
				bjcode			<= i_IDRN_bjcode;
				iecode			<= i_IDRN_iecode;
				mvcode			<= i_IDRN_mvcode;
				mdcode			<= i_IDRN_mdcode;
				memcode			<= i_IDRN_memcode;
				regsrc1			<= i_IDRN_regsrc1;
				regsrc2			<= i_IDRN_regsrc2;
				regdes			<= i_IDRN_regdes;
				BranchTaken		<= i_IDRN_BranchTaken;
				BranchTarget	<= i_IDRN_BranchTarget;
				EXCCODE			<= i_IDRN_EXCCODE;
			end
	end

endmodule