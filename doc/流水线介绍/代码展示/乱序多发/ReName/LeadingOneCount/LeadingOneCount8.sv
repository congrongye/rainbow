module LeadingOneCount8(

	input	[7:0]	i_LOC8_datain,
	
	output			o_LOC8_vld,
	output	[2:0]	o_LOC8_cnt

);

	wire			hi_vld;
	wire	[1:0]	hi_cnt;
	wire			lo_vld;
	wire	[1:0]	lo_cnt;
	
	assign	o_LOC8_vld = hi_vld | lo_vld;
	assign	o_LOC8_cnt = (hi_vld) ? {1'b1, hi_cnt} : {1'b0, lo_cnt};

LeadingOneCount4 LOC8_LOC4_hi(
	.i_LOC4_datain(	i_LOC8_datain[7:4]),
	.o_LOC4_vld(	hi_vld),
	.o_LOC4_cnt(	hi_cnt)
	
);

LeadingOneCount4 LOC8_LOC4_lo(
	.i_LOC4_datain(	i_LOC8_datain[3:0]),
	.o_LOC4_vld(	lo_vld),
	.o_LOC4_cnt(	lo_cnt)
);

endmodule