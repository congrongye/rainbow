module LeadingOneCount2(
	
	input	[1:0]	i_LOC2_datain,
	
	output			O_LOC2_vld,
	output			O_LOC2_cnt

);
	
	assign	O_LOC2_vld = i_LOC2_datain[1] | i_LOC2_datain[0];
	assign	O_LOC2_cnt = i_LOC2_datain[1];

endmodule