module LeadingOneCount32(

	input	[31:0]	i_LOC32_datain,
	
	output			o_LOC32_vld,
	output	[4:0]	o_LOC32_cnt

);
	
	wire			hi_vld;
	wire	[3:0]	hi_cnt;
	wire			lo_vld;
	wire	[3:0]	lo_cnt;
	
	assign	o_LOC32_vld = hi_vld | lo_vld;
	assign	o_LOC32_cnt = (hi_vld) ? {1'b1, hi_cnt} : {1'b0, lo_cnt};

LeadingOneCount16 LOC32_LOC16_hi(
	.i_LOC16_datain(i_LOC32_datain[31:16]),
	.o_LOC16_vld(	hi_vld),
	.o_LOC16_cnt(	hi_cnt)
);

LeadingOneCount16 LOC32_LOC16_hi(
	.i_LOC16_datain(i_LOC32_datain[15:0]),
	.o_LOC16_vld(	lo_vld),
	.o_LOC16_cnt(	lo_cnt)
);

endmodule