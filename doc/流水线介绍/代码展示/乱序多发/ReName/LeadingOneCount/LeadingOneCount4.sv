module LeadingOneCount4(
	
	input	[3:0]	i_LOC4_datain,
	
	output			o_LOC4_vld,
	output	[1:0]	o_LOC4_cnt
	
);

	wire			hi_vld;
	wire			hi_cnt;
	wire			lo_vld;
	wire			lo_cnt;
	
	assign	o_LOC4_vld = hi_vld | lo_vld;
	assign	o_LOC4_cnt = (hi_vld) ? {1'b1, hi_cnt} : {1'b0, lo_cnt};

LeadingOneCount2 LOC4_LOC2_hi(
	.i_LOC2_datain(	i_LOC4_datain[3:2]),
	.o_LOC2_vld(	hi_vld),
	.o_LOC2_cnt(	hi_cnt)
);

LeadingOneCount2 LOC4_LOC2_lo(
	.i_LOC2_datain(	i_LOC4_datain[1:0]),
	.o_LOC2_vld(	lo_vld),
	.o_LOC2_cnt(	lo_cnt)
);

endmodule