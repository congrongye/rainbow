module LeadingOneCount64(

	input	[63:0]	i_LOC64_datain,
	
	output			o_LOC64_vld,
	output	[5:0]	o_LOC64_cnt

);

	wire			hi_vld;
	wire	[4:0]	hi_cnt;
	wire			lo_vld;
	wire	[4:0]	lo_cnt;
	
	assign	o_LOC64_vld = hi_vld | lo_vld;
	assign	o_LOC64_cnt = (hi_vld) ? {1'b1, hi_cnt} : {1'b0, lo_cnt};

LeadingOneCount32 LOC64_LOC32_hi(
	.i_LOC32_datain(i_LOC64_datain[63:32]),
	.o_LOC32_vld(	hi_vld),
	.o_LOC32_cnt(	hi_cnt)
);

LeadingOneCount32 LOC64_LOC32_lo(
	.i_LOC32_datain(i_LOC64_datain[31:0]),
	.o_LOC32_vld(	lo_vld),
	.o_LOC32_cnt(	lo_cnt)
);

endmodule