module LeadingOneCount16(

	input	[15:0]	i_LOC16_datain,
	
	output			o_LOC16_vld,
	output	[3:0]	o_LOC16_cnt

);

	wire			hi_vld;
	wire	[2:0]	hi_cnt;
	wire			lo_vld;
	wire	[2:0]	lo_cnt;
	
	assign	o_LOC16_vld = hi_vld | lo_vld;
	assign	o_LOC16_cnt = (hi_vld) ? {1'b1, hi_cnt} : {1'b0, lo_cnt};

LeadingOneCount8 LOC16_LOC8_hi(
	.i_LOC8_datain(	i_LOC16_datain[15:8]),
	.o_LOC8_vld(	hi_vld),
	.o_LOC8_cnt(	hi_cnt)
);

LeadingOneCount8 LOC16_LOC8_lo(
	.i_LOC8_datain(	i_LOC16_datain[7:0]),
	.o_LOC8_vld(	lo_vld),
	.o_LOC8_cnt(	lo_cnt)
);

endmodule