module RenameTable(
	
	input			i_RT_clk,
	input			i_RT_enable,
	input			i_RT_reset,
	//regsrc
	input	[4:0]	i_RT_regsrc1,
	input	[4:0]	i_RT_regsrc2,
	input	[4:0]	i_RT_regsrc3,
	input	[4:0]	i_RT_regsrc4,
	output	[5:0]	o_RT_pregsrc1,
	output	[5:0]	o_RT_pregsrc2,
	output	[5:0]	o_RT_pregsrc3,
	output	[5:0]	o_RT_pregsrc4,
	//regdes
	input	[4:0]	i_RT_regdes1,
	input	[4:0]	i_RT_regdes2,
	output	[5:0]	o_RT_ppregdes1,
	output	[5:0]	o_RT_ppregdes2,
	input	[5:0]	i_RT_pregdes1,
	input	[5:0]	i_RT_pregdes2,
	//COMMIT
	input	[4:0]	i_RT_CMTregdes1,
	input	[4:0]	i_RT_CMTregdes2,
	input	[5:0]	i_RT_CMTpregdes1,
	input	[5:0]	i_RT_CMTpregdes2,
	input			i_RT_CMTrollback

);

	reg		[5:0]	PhyRegNum[31:0];
	reg		[5:0]	PhyRegNumRec[31:0];	
	
	assign	o_RT_pregsrc1 = PhyRegNum[i_RT_regsrc1];
	assign	o_RT_pregsrc2 = PhyRegNum[i_RT_regsrc2];
	assign	o_RT_pregsrc3 = PhyRegNum[i_RT_regsrc3];
	assign	o_RT_pregsrc4 = PhyRegNum[i_RT_regsrc4];

	assign	o_RT_ppregdes1 = PhyRegNum[i_RT_regdes1];
	assign	o_RT_ppregdes2 = PhyRegNum[i_RT_regdes2];
	
always @(posedge i_RT_clk)
	begin
		if(i_RT_reset)
			begin:	reset
				reg	[7:0]	tempI;
				for(tempI=8'd0; tempI<=8'd31; tempI=tempI+8'd1)
					begin
						PhyRegNum[tempI[4:0]] <= tempI[4:0];
						PhyRegNumRec[tempI[4:0]] <= tempI[4:0];
					end
			end
		else if(i_RT_enable)
			begin
				if(i_RT_CMTrollback)
					begin:	rollback
						reg	[7:0]	tempJ;
						for(tempJ=8'd0; tempJ<=8'd31; tempJ=tempJ+8'd1)
							PhyRegNum[tempJ[4:0]] <= PhyRegNumRec[tempJ[4:0]];
					end
				else
					begin
						if(i_RT_regdes1) PhyRegNum[i_RT_regdes1] <= i_RT_pregdes1;
						if(i_RT_regdes2) PhyRegNum[i_RT_regdes2] <= i_RT_pregdes2;
						
						if(i_RT_CMTregdes1) PhyRegNumRec[i_RT_CMTregdes1] <= i_RT_CMTpregdes1;
						if(i_RT_CMTregdes2) PhyRegNumRec[i_RT_CMTregdes2] <= i_RT_CMTpregdes2;
					end
			end
	end

endmodule