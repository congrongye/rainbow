module IFPID(
	
	input			i_IFPID_clk,
	input			i_IFPID_enable,
	input			i_IFPID_reset,
	
	input			i_IFPID_valid,
	input	[31:0]	i_IFPID_PC,
	input			i_IFPID_BranchHit,
	input	[1:0]	i_IFPID_BranchTaken,
	input	[31:0]	i_IFPID_BranchTarget,
	
	output			o_IFPID_valid,
	output	[31:0]	o_IFPID_PC,
	output			o_IFPID_BranchHit,
	output	[1:0]	o_IFPID_BranchTaken,
	output	[31:0]	o_IFPID_BranchTarget
	
);

	reg			valid;
	reg	[31:0]	PC;
	reg			BranchHit;
	reg	[1:0]	BranchTaken;
	reg	[31:0]	BranchTarget;

	assign	o_IFPID_valid			= valid;
	assign	o_IFPID_PC				= PC;
	assign	o_IFPID_BranchHit		= BranchHit;
	assign	o_IFPID_BranchTaken		= BranchTaken;
	assign	o_IFPID_BranchTarget	= BranchTarget;

always @(posedge i_IFPID_clk)
	begin
		if(i_IFPID_reset)
			begin
				valid			<= 1'd0;
				PC				<= 32'd0;
				BranchHit		<= 1'd0;
				BranchTaken		<= 2'd0;
				BranchTarget	<= 32'd0;
			end
		else if(i_IFPID_enable)
			begin
				valid			<= i_IFPID_valid;
				PC				<= i_IFPID_PC;
				BranchHit		<= i_IFPID_BranchHit;
				BranchTaken		<= i_IFPID_BranchTaken;
				BranchTarget	<= i_IFPID_BranchTarget;
			end
	end

endmodule