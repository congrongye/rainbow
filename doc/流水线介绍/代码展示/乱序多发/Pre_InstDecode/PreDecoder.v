module PreDecoder(

	input	[31:0]	i_PDC_PC1,
	input	[31:0]	i_PDC_Inst1,
	input	[31:0]	i_PDC_PC2,
	input	[31:0]	i_PDC_Inst2,
	
	//Inst1
	output			o_PDC_BJvalid1,
	output			o_PDC_likely1,
	output	[31:0]	o_PDC_PC1,
	output	[31:0]	o_PDC_target1,
	output	[31:0]	o_PDC_Inst1,
	//Inst2
	output			o_PDC_BJvalid2,
	output			o_PDC_likely2,
	output	[31:0]	o_PDC_PC2,
	output	[31:0]	o_PDC_target2,
	output	[31:0]	o_PDC_Inst2
	
);
	
	wire	[5:0]	opcode1 = i_DC_Inst1[31:26];
	wire	[4:0]	sa1 	= i_DC_Inst1[10:6];
	wire	[5:0]	func1	= i_DC_Inst1[5:0];
	wire	[4:0]	rs1	= i_DC_Inst1[25:21];
	wire	[4:0]	rt1	= i_DC_Inst1[20:16];
	wire	[4:0]	rd1	= i_DC_Inst1[15:11];
	
	wire	BEQ_1		= (opcode1==6'b000100);
	wire	BGTZ_1		= (opcode1==6'b000111 && rt1==5'b00000);
	wire	BGEZ_1		= (opcode1==6'b000001 && rt1==5'b00001);
	wire	BGEZAL_1	= (opcode1==6'b000001 && rt1==5'b10001);
	wire	BNE_1		= (opcode1==6'b000101);
	wire	BLEZ_1		= (opcode1==6'b000110 && rt1==5'b00000);
	wire	BLTZ_1		= (opcode1==6'b000001 && rt1==5'b00000);
	wire	BLEZAL_1	= (opcode1==6'b000001 && rt1==5'b10000);
	wire	J_1			= (opcode1==6'b000010);
	wire	JAL_1		= (opcode1==6'b000011);
	wire	JR_1		= (opcode1==6'b000000 && rt1==5'b00000 && rd1==5'b00000 && sa1==5'b00000 && func1==6'b001000);
	wire	JALR_1		= (opcode1==6'b000000 && rt1==5'b00000 && sa1==5'b00000 && func1==6'b001001);
	
	wire	[5:0]	opcode2 = i_DC_Inst2[31:26];
	wire	[4:0]	sa2 	= i_DC_Inst2[10:6];
	wire	[5:0]	func2	= i_DC_Inst2[5:0];
	wire	[4:0]	rs2	= i_DC_Inst2[25:21];
	wire	[4:0]	rt2	= i_DC_Inst2[20:16];
	wire	[4:0]	rd2	= i_DC_Inst2[15:11];
	
	wire	BEQ_2		= (opcode2==6'b000100);
	wire	BGTZ_2		= (opcode2==6'b000111 && rt2==5'b00000);
	wire	BGEZ_2		= (opcode2==6'b000001 && rt2==5'b00001);
	wire	BGEZAL_2	= (opcode2==6'b000001 && rt2==5'b10001);
	wire	BNE_2		= (opcode2==6'b000101);
	wire	BLEZ_2		= (opcode2==6'b000110 && rt2==5'b00000);
	wire	BLTZ_2		= (opcode2==6'b000001 && rt2==5'b00000);
	wire	BLEZAL_2	= (opcode2==6'b000001 && rt2==5'b10000);
	wire	J_2			= (opcode2==6'b000010);
	wire	JAL_2		= (opcode2==6'b000011);
	wire	JR_2		= (opcode2==6'b000000 && rt2==5'b00000 && rd2==5'b00000 && sa2==5'b00000 && func2==6'b001000);
	wire	JALR_2		= (opcode2==6'b000000 && rt2==5'b00000 && sa2==5'b00000 && func2==6'b001001);
	
	//Inst1
	assign	o_PDC_BJvalid1	= BEQ_1 |BGTZ_1 |BGEZ_1 |BGEZAL_1 |J_1	|JAL_1	|JR_1	|JALR_1;
	assign	o_PDC_likely1	= 1'd0;
	assign	o_PDC_PC1		= i_PDC_PC1;
	assign	o_PDC_target1	= i_PDC_PC1 + 32'd4 + {14{i_DC_Inst1[15]}, i_DC_Inst1[15:0], 2'b00};
	assign	o_PDC_Inst1		= i_DC_Inst1;
	//Inst2
	assign	o_PDC_BJvalid2	= BEQ_2	|BGTZ_2 |BGEZ_2 |BGEZAL_2 |J_2	|JAL_2	|JR_2	|JALR_2;
	assign	o_PDC_likely2	= 1'd0;
	assign	o_PDC_PC2		= (o_PDC_valid2) ? 32'd0 : i_PDC_PC2;
	assign	o_PDC_target2	= 32'd0;
	assign	o_PDC_Inst2		= (o_PDC_valid2) ? 32'd0 : i_DC_Inst2;
	
endmodule