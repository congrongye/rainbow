//MyCPU.scala
package test

import chisel3._
import chisel3.util._

class MyCPU extends Module{
  val io = IO(new Bundle{
    //val inst = Input(UInt(32.W))

    //val debug_port_BJ_vld = Output(Bool())
    //val debug_port_BJ_target = Output(UInt(32.W))
    //val debug_port_wb_PC = Output(UInt(32.W))
    //val debug_port_wb_wen = Output(Bool())
    //val debug_port_wb_regdes = Output(UInt(5.W))
    //val debug_port_wb_wbdata = Output(UInt(32.W))
    
    //Show the results of lab1 code about vector dot products
    val select = Input(UInt(4.W))
    val results = Output(UInt(4.W))
  })

  val IF = Module(new InstFetch)
  val ID = Module(new InstDecode)
  val EX = Module(new Execute)
  val MEM = Module(new Memory)
  val WB = Module(new WriteBack)
  
//Show the results of lab1 code about vector dot products
  ID.io.select := io.select
  io.results := ID.io.results
//Show the results of lab1 code about vector dot products

  //data path
  IF.io.ifid <> ID.io.ifid
  ID.io.idex <> EX.io.idex
  EX.io.exmem <> MEM.io.exmem
  MEM.io.memwb <> WB.io.memwb

  //write back
  WB.io.wbid <> ID.io.wbid

  //branch and jump
  ID.io.BJ <> IF.io.BJ

  //redirection
  EX.io.RD_ex <> ID.io.RD_ex
  MEM.io.RD_mem <> ID.io.RD_mem
  WB.io.RD_wb <> ID.io.RD_wb

  //bubble
  val bubble = ID.io.Bubble_id && EX.io.Bubble_ex
  IF.io.enable := ~bubble
  ID.io.enable := ~bubble

  //io.debug_port_BJ_vld := IF.io.BJ.vld
  //io.debug_port_BJ_target := IF.io.BJ.target
  //io.debug_port_wb_PC := ID.io.wbid.PC
  //io.debug_port_wb_wen := ID.io.wbid.wen
  //io.debug_port_wb_regdes := ID.io.wbid.regdes
  //io.debug_port_wb_wbdata := ID.io.wbid.wbdata
}

