//ALU.scala
package test

import chisel3._
import chisel3.util._

class ALU{
  def add(data1: UInt, data2: UInt): UInt = { data1 + data2 }
  def sub(data1: UInt, data2: UInt): UInt = { data1 - data2 }
  def lui(imm3112: UInt):UInt = { SignExt(imm3112 << 0x0c, 32).asUInt }
  def auipc(PC: UInt, imm3112: UInt): UInt = { PC + SignExt(imm3112 << 0x0c, 32).asUInt }

  def and(data1: UInt, data2: UInt): UInt = { data1 & data2 }
  def xor(data1: UInt, data2: UInt): UInt = { data1 ^ data2 }
  def or(data1: UInt, data2: UInt): UInt = { data1 | data2 }

  def slt(data1: UInt, data2: UInt): UInt = { data1.asSInt < data2.asSInt }
  def sltu(data1: UInt, data2: UInt): UInt = { data1 < data2 }

  def sll(data1: UInt, data2: UInt): UInt = { (data1 << data2)(31,0) }
  def srl(data1: UInt, data2: UInt): UInt = { data1 >> data2 }
  def sra(data1: UInt, data2: UInt): UInt = { (data1.asSInt >> data2).asUInt }

  def mul(data1: UInt, data2: UInt): UInt = { (data1 * data2)(31, 0) }
}

