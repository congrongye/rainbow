//InstFetch.scala
package test

import chisel3._
import chisel3.util._
import chisel3.util.experimental.loadMemoryFromFile

class InstMemory{
  val imem = Mem(256, UInt(32.W))
  //loadMemoryFromFile(imem, "/home/yecr/MyRISCV/chisel/chisel-template/src/test/microtest.txt")
  loadMemoryFromFile(imem, "/home/yecr/MyRISCV/chisel/chisel-template/src/test/lab1_vdp.txt")
  //loadMemoryFromFile(imem, "/home/yecr/MyRISCV/chisel/chisel-template/src/test/password.txt")
  def read(addr: UInt): UInt = { imem(addr) }
}

class InstFetch extends Module{
  val io = IO(new Bundle{
   // val inst = Input(UInt(32.W))
    val ifid = Output(new IFID)

    val BJ = Input(new BranchJump)
    val enable = Input(Bool())
  })

  val PC = RegInit(0.U(32.W))
  when(io.enable){

    when(io.BJ.vld){
      PC := io.BJ.target
    }otherwise{
      PC := PC + 4.U
    }

  }

  val instmemory = new InstMemory
  val INST = instmemory.read(PC(10,2))

  io.ifid.Valid := true.B
  io.ifid.Inst := Mux(io.BJ.vld, 0x00000013.U, INST)
  io.ifid.PC := PC

}

