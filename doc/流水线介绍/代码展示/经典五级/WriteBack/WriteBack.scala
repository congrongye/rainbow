//WriteBack.scala
package test

import chisel3._
import chisel3.util._

class WriteBack extends Module{
  val io = IO(new Bundle{
    val memwb = Input(new MEMWB)
    val wbid = Output(new WBID)

    val RD_wb = Output(new ReDirection)
  })

  val wbs = RegNext(io.memwb)

  val jal  = (wbs.Inst === BitPat("b???????_?????_?????_???_?????_1101111"))
  val jalr = (wbs.Inst === BitPat("b???????_?????_?????_000_?????_1100111"))
  val jump = jal || jalr

  io.wbid.Valid := wbs.Valid
  io.wbid.Inst := wbs.Inst
  io.wbid.PC := wbs.PC
  io.wbid.wbdata := Mux(jump, (wbs.PC + 4.U), wbs.wbdata)
  io.wbid.wen := wbs.wen
  io.wbid.regdes := wbs.regdes

  io.RD_wb.wen := wbs.wen
  io.RD_wb.regdes := wbs.regdes
  io.RD_wb.wbdata := io.wbid.wbdata
}

