//Bundle.scala
package test

import chisel3._
import chisel3.util._

trait VaildInstPC{
  val Valid = Bool()
  val Inst = UInt()
  val PC = UInt()
}

abstract class MyBundle extends Bundle with VaildInstPC

class IFID extends MyBundle{}

class IDEX extends MyBundle{
  val aluop = UInt()
  val src1 = UInt()
  val src2 = UInt()
  val simm = UInt()
  val shamt = UInt()

  val wen = Bool()
  val regdes =UInt()
}

class EXMEM extends MyBundle{
  val aluresult = UInt()

  val wen = Bool()
  val regdes =UInt()

  val load = Bool()
  val store = Bool()
  val storeData = UInt()
}

class MEMWB extends MyBundle{
  val wbdata = UInt()
  val wen = Bool()
  val regdes =UInt()
}

class WBID extends MyBundle{
  val wbdata = UInt()
  val wen = Bool()
  val regdes =UInt()
}

class ReDirection extends Bundle{
  val wen = Bool()
  val regdes = UInt()
  val wbdata = UInt()
}

class BranchJump extends Bundle{
  val vld = Bool()
  val target = UInt()
}

//from NutShell
object SignExt{
  def apply(data: UInt, len: Int) = {
    val aLen = data.getWidth
    val signBit = data(aLen-1)
    if (aLen >= len) data(len-1,0) else Cat(Fill(len - aLen, signBit), data)
  }
}

//from NutShell
object ZeroExt{
  def apply(data: UInt, len: Int) = {
    val aLen = data.getWidth
    if (aLen >= len) data(len-1,0) else Cat(0.U((len - aLen).W), data)
  }
}

