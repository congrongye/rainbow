//RegFile.scala
package test

import chisel3._
import chisel3.util._

class RegFile{
  val rf = Mem(32, UInt(32.W))
  def read(addr: UInt): UInt = { Mux(addr === 0.U, 0.U, rf(addr)) }
  def write(wen: Bool, addr: UInt, data: UInt): Unit = { rf(Mux(wen, addr, 0.U)) := data }
}

