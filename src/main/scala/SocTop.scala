//SocTop.scala
package sim

import chisel3._
import chisel3.util._
import mycore._

class ysyx_210292 extends Module {
  val io = IO(new Bundle {
  	val interrupt = Input(Bool())
    val master = new SocAXI
    val slave = Flipped(new SocAXI)
  })

  val AbstractCore = Module(new AbstractCore)
  val AXICare = Module(new AXICare)
  AbstractCore.io.TIME <> AXICare.io.TIME
  AbstractCore.io.ramhelperIO <> AXICare.io.ramhelperIO

  AbstractCore.io.isSoC := true.B
  AXICare.io.isSoC := true.B

  io.master.awready <> AXICare.io.MYAXI.AWREADY
  io.master.awvalid <> AXICare.io.MYAXI.AWVALID
  io.master.awaddr <> AXICare.io.MYAXI.AWADDR(31,0)
  io.master.awid <> 0.U
  io.master.awlen <> 0.U
  io.master.awsize <> AXICare.io.MYAXI.AWSIZE
  io.master.awburst <> 1.U

  io.master.wready <> AXICare.io.MYAXI.WREADY
  io.master.wvalid <> AXICare.io.MYAXI.WVALID
  io.master.wdata <> AXICare.io.MYAXI.WDATA
  io.master.wstrb <> AXICare.io.MYAXI.WSTRB
  io.master.wlast <> 1.U

  io.master.bready <> AXICare.io.MYAXI.BREADY
  io.master.bvalid <> AXICare.io.MYAXI.BVALID
  //io.master.bresp <> 
  //io.master.bid <>   

  io.master.arready <> AXICare.io.MYAXI.ARREADY
  io.master.arvalid <> AXICare.io.MYAXI.ARVALID
  io.master.araddr <> AXICare.io.MYAXI.ARADDR(31,0)
  io.master.arid <> 0.U
  io.master.arlen <> 0.U
  io.master.arsize <> AXICare.io.MYAXI.ARSIZE
  io.master.arburst <> 1.U

  io.master.rready <> AXICare.io.MYAXI.RREADY
  io.master.rvalid <> AXICare.io.MYAXI.RVALID
  io.master.rdata <> AXICare.io.MYAXI.RDATA
  //io.master.rresp <>
  //io.master.rlast <> 
  //io.master.rid <>

  io.slave.awready <> false.B
  io.slave.wready <> false.B
  io.slave.bvalid <> false.B
  io.slave.bresp <> 0.U
  io.slave.bid <> 0.U
  io.slave.arready <> false.B
  io.slave.rvalid <> false.B
  io.slave.rresp <> 0.U
  io.slave.rdata <> 0.U
  io.slave.rlast <> false.B
  io.slave.rid <> 0.U

}

class SocAXI extends Bundle {

  val awready = Input(Bool())
  val awvalid = Output(Bool())
  val awaddr = Output(UInt(32.W))
  val awid = Output(UInt(4.W))
  val awlen = Output(UInt(8.W))
  val awsize = Output(UInt(3.W))
  val awburst = Output(UInt(2.W))

  val wready = Input(Bool())
  val wvalid = Output(Bool())
  val wdata = Output(UInt(64.W))
  val wstrb = Output(UInt(8.W))
  val wlast = Output(Bool())

  val bready = Output(Bool())
  val bvalid = Input(Bool())
  val bresp = Input(UInt(2.W))
  val bid = Input(UInt(4.W))

  val arready = Input(Bool())
  val arvalid = Output(Bool())
  val araddr = Output(UInt(32.W))
  val arid = Output(UInt(4.W))
  val arlen = Output(UInt(8.W))
  val arsize = Output(UInt(3.W))
  val arburst = Output(UInt(2.W))

  val rready = Output(Bool())
  val rvalid = Input(Bool())
  val rresp = Input(UInt(2.W))
  val rdata = Input(UInt(64.W))
  val rlast = Input(Bool())
  val rid = Input(UInt(4.W))

}