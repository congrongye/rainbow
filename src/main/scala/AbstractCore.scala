//AbstractCore.scala
package sim

import chisel3._
import chisel3.util._
import mycore._

class AbstractCore extends Module {
  val io = IO(new Bundle {

    /*--------------------------------------------------------------------------------------------*/
  	/*--This is a very, very, very critical signal that needs to be rigorously tested separately--*/
  	/*--------------------------------------------------------------------------------------------*/
  	val TIME = Input(Bool())
  	/*--------------------------------------------------------------------------------------------*/

  	val ramhelperIO = new RAMHelperIO
    val InstrCommit = Flipped(new DiffInstrCommitIO)
    val ArchIntRegState = Flipped(new DiffArchIntRegStateIO)
    val CSRState = Flipped(new DiffCSRStateIO)
    val TrapEvent = Flipped(new DiffTrapEventIO)
    val ArchFpRegState = Flipped(new DiffArchFpRegStateIO)
    val ArchEvent = Flipped(new DiffArchEventIO)

    val isSoC = Input(Bool())
  	
  })

  val mycore = Module(new Rainbow)
  val iagent = Module(new InterfaceAgent)

  mycore.io.ENABLE <> iagent.io.mycoreIO.ENABLE
  mycore.io.InstFetch <> iagent.io.mycoreIO.InstFetch
  mycore.io.DataLoad <> iagent.io.mycoreIO.DataLoad
  mycore.io.DataStore <> iagent.io.mycoreIO.DataStore
  iagent.io.ramhelperIO <> io.ramhelperIO

  mycore.io.InstrCommit <> io.InstrCommit
  mycore.io.ArchIntRegState <> io.ArchIntRegState
  mycore.io.CSRState <> io.CSRState
  mycore.io.TrapEvent <> io.TrapEvent
  mycore.io.ArchFpRegState <> io.ArchFpRegState
  mycore.io.ArchEvent <> io.ArchEvent

  iagent.io.TIME := io.TIME

  mycore.io.isSoC := io.isSoC
  iagent.io.isSoC := io.isSoC

}