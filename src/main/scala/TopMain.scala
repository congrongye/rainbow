package top

import chisel3.stage._
import sim._

object TopMain extends App {
  (new ChiselStage).execute(
    args,
    Seq(
      //ChiselGeneratorAnnotation(() => new SimTop()),
      ChiselGeneratorAnnotation(() => new ysyx_210292()), firrtl.stage.RunFirrtlTransformAnnotation(new AddModulePrefix()), ModulePrefixAnnotation("ysyx_210292_")
    )
  )
}
